# -*- coding: utf-8 -*-

"""
Madminer parameter and morphing setup code for a WH signal.

Includes the dominant CP-even operators (oHW, oHq3), the CP-odd operator (oHWtil), and two subdominant CP-even operators (oHbox, oHDD).

Ricardo Barrue (LIP/IST/CERN-ATLAS), 15/2/2020
"""


from __future__ import absolute_import, division, print_function, unicode_literals

import os
import logging
import numpy as np
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt

# MadMiner output
logging.basicConfig(
    format='%(asctime)-5.5s %(name)-20.20s %(levelname)-7.7s %(message)s',
    datefmt='%H:%M',
    level=logging.INFO
)

# Output of all other modules (e.g. matplotlib)
for key in logging.Logger.manager.loggerDict:
    if "madminer" not in key:
        logging.getLogger(key).setLevel(logging.WARNING)

from madminer import MadMiner,plot_nd_morphing_basis_scatter

# Partition to store h5 files and samples 
main_proc_dir = '/lstore/calo/rbarrue/MadminerWHStudies/all' # Pauli

if __name__ == "__main__":

  # Instance of MadMiner core class
  miner = MadMiner()

  miner.add_parameter(
      lha_block='smeft',
      lha_id=4,
      parameter_name='cHbox',
      morphing_max_power=2,
      parameter_range=(-1.0,1.0)
  )

  miner.add_parameter(
      lha_block='smeft',
      lha_id=5,
      parameter_name='cHDD',
      morphing_max_power=2,
      parameter_range=(-1.0,1.0)
  )

  miner.add_parameter(
      lha_block='smeft',
      lha_id=7,
      parameter_name='cHW',
      morphing_max_power=2,
      param_card_transform="0.1*theta",
      parameter_range=(-5.0,5.0)
  )

  miner.add_parameter(
      lha_block='smeft',
      lha_id=25,
      parameter_name='cHq3',
      morphing_max_power=2,
      param_card_transform="0.01*theta",
      parameter_range=(-5.0,5.0)
  )

  miner.add_parameter(
      lha_block='smeftcpv',
      lha_id=4,
      parameter_name='cHWtil',
      morphing_max_power=1, # interference only
      parameter_range=(-1.0,1.0)
  )

  # Only want the SM benchmark specifically - let Madminer choose the others
  miner.add_benchmark({'cHbox':0.000000,'cHDD':0.000000,'cHW':0.000000,'cHq3':0.000000,'cHWtil':0.000000},'sm')

  # Morphing - automatic optimization to avoid large weights
  miner.set_morphing(max_overall_power=2,include_existing_benchmarks=True)

  morphing_weight_plots = plot_nd_morphing_basis_scatter(miner.morpher,n_test_thetas=100000)
  morphing_weight_plots.savefig('{}/morphing_weights_all.png'.format(main_proc_dir))

  miner.save('{}/setup_all.h5'.format(main_proc_dir))