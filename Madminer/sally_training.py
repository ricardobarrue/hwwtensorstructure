# -*- coding: utf-8 -*-

"""
sally_training.py

Creates training samples for the a local score-based method (SALLY).

Uses SampleAugmenter to extract training (and test) samples and joint score.

Trains an ensemble of score estimators (to have an idea of the uncertainty from the different NN trainings)

Ricardo Barrué (LIP/IST/CERN-ATLAS), 13/3/2021
"""

from __future__ import absolute_import, division, print_function, unicode_literals
import logging

import sys
import os

from madminer.sampling import SampleAugmenter
from madminer import sampling
from madminer.ml import ScoreEstimator, Ensemble

# MadMiner output
logging.basicConfig(
  format='%(asctime)-5.5s %(name)-20.20s %(levelname)-7.7s %(message)s',
  datefmt='%H:%M',
  level=logging.INFO
)

# Output of all other modules (e.g. matplotlib)
for key in logging.Logger.manager.loggerDict:
  if "madminer" not in key:
    logging.getLogger(key).setLevel(logging.WARNING)

# Partition to store h5 files and samples + path to setup file
main_proc_dir= '/users3/t3atlas/rbarrue/MadminerWHStudies/CPoddOnly_no_ang_obs' # Cirrus
#main_proc_dir = '/lstore/calo/rbarrue/MadminerWHStudies/CPoddOnly_no_ang_obs' # Pauli
setup_file='{}/setup_CPoddOnly.h5'.format(main_proc_dir)

# mode: 'augment_only': does only the data augmentation, 'train_only': does only training, 'augment_and_train': does augmentation and training in one go
# train_estimators_parallel: trains all estimators at once, with Ensemble.train_all(). If false, trains estimators serially
def augment_and_train(channel,observables,nestimators,nsamples,mode='augment_and_train',train_estimators_parallel=True):

  # Exiting if mode not on list of available modes
  if mode.lower() not in ['augment_only','train_only','augment_and_train']:
    sys.exit('augment_and_train: running mode {} not possible. Valid modes are augment_only, train_only or augment_and_train (augmentation+training in one go)'.format(mode))
  else:
    print('augment_and_train: running with mode {}'.format(mode))

  print('augment_and_train: channel: {}; observable set: {}'.format(channel,observables))

  # TODO: change this when introducing additional observables (or if deciding if I want to study single observables)
  if observables == 'ptw':
    sampler_obs = 'met'
  else:
    sampler_obs = observables # larger set of observables - can choose any of the features therein to train the network

  if mode.lower() in ['augment_only','augment_and_train']: 
    ########## Sample Augmentation ###########

    # object to create the augmented training samples
    sampler=SampleAugmenter('{}/{}/{}_{}.h5'.format(main_proc_dir,sampler_obs,channel,sampler_obs))

    # Creates a set of training data (as many as the number of estimators) - centered around the SM
    # Samples from all benchmarks to take into account the different kinematics from CP-odd operator
    for i in range(nestimators):
      _,_,_,eff_n_samples = sampler.sample_train_local(theta=sampling.benchmark('sm'),
                                        n_samples=int(nsamples),
                                        folder='{}/{}/training_samples/'.format(main_proc_dir,sampler_obs),
                                        filename='train_score_{}_{}'.format(channel,i),
                                        sample_only_from_closest_benchmark=False)
    
    print('augment_and_train: effective number of samples for estimator {}: {}'.format(i,eff_n_samples))

  if mode.lower() in ['train_only','augment_and_train']:

    ########## Training ###########

    # Choose which features to train on 
    # If 'met' or 'full', we use all of them (None), otherwise we select the correct indices
    # TODO: change this when introducing additional observables (or if deciding if I want to study single observables)
    if observables == 'met' or 'full':
        my_features = None
    elif observables == 'ptw':
        my_features = [18]
    elif observables == '2d':
        my_features = [18, 39]
    
    #Create a list of ScoreEstimator objects to add to the ensemble
    # TODO: in the long term future, optimize the NN settings
    estimators = [ ScoreEstimator(features=my_features, n_hidden=(50,)) for _ in range(nestimators) ]
    ensemble = Ensemble(estimators)

    # Run the training of the ensemble
    # result is a list of N tuples, where N is the number of estimators,
    # and each tuple contains two arrays, the first with the training losses, the second with the validation losses
    # TODO: optimize training parameters
    
    result = ensemble.train_all(method='sally',
      x=['{}/{}/training_samples/x_train_score_{}_{}.npy'.format(main_proc_dir,sampler_obs,channel,i) for i in range(nestimators)],
      t_xz=['{}/{}/training_samples/t_xz_train_score_{}_{}.npy'.format(main_proc_dir,sampler_obs,channel,i) for i in range(nestimators)],
      memmap=True,verbose="all",n_workers=1,limit_samplesize=nsamples
      )    

    ensemble.save('{}/{}/models/sally_ensemble_{}_{}'.format(main_proc_dir,sampler_obs,channel,observables),save_model=True)
  
if __name__ == "__main__":

  # Run the data augmentation and the method training

  # mode: 'augment_only': does only the data augmentation, 'train_only': does only training, 'augment_and_train': does augmentation and training in one go

  run_mode='augment_only'

  for observables in ['full','met']:

    # W+ -> mu+ vm
    augment_and_train('wph_mu_signalOnly_SMonly_noSysts_lhe',observables,5,250000,mode=run_mode)
    augment_and_train('wph_mu_signalOnly_noSysts_lhe',observables,5,250000+50000,mode=run_mode)
    augment_and_train('wph_mu_withBackgrounds_SMonly_noSysts_lhe',observables,5,250000+3*250000,mode=run_mode)
    augment_and_train('wph_mu_withBackgrounds_noSysts_lhe',observables,5,250000+50000+3*250000,mode=run_mode)

    # W+ -> e+ ve
    augment_and_train('wph_e_signalOnly_SMonly_noSysts_lhe',observables,5,250000,mode=run_mode)
    augment_and_train('wph_e_signalOnly_noSysts_lhe',observables,5,250000+50000,mode=run_mode)
    augment_and_train('wph_e_withBackgrounds_SMonly_noSysts_lhe',observables,5,250000+3*250000,mode=run_mode)
    augment_and_train('wph_e_withBackgrounds_noSysts_lhe',observables,5,250000+50000+3*250000,mode=run_mode)

    # W- -> mu- vm~
    augment_and_train('wmh_mu_signalOnly_SMonly_noSysts_lhe',observables,5,250000,mode=run_mode)
    augment_and_train('wmh_mu_signalOnly_noSysts_lhe',observables,5,250000+50000,mode=run_mode)
    augment_and_train('wmh_mu_withBackgrounds_SMonly_noSysts_lhe',observables,5,250000+3*250000,mode=run_mode)
    augment_and_train('wmh_mu_withBackgrounds_noSysts_lhe',observables,5,250000+50000+3*250000,mode=run_mode)

    # W- -> e- ve~
    augment_and_train('wmh_e_signalOnly_SMonly_noSysts_lhe',observables,5,250000,mode=run_mode)
    augment_and_train('wmh_e_signalOnly_noSysts_lhe',observables,5,250000+50000,mode=run_mode)
    augment_and_train('wmh_e_withBackgrounds_noSysts_lhe',observables,5,250000+50000+3*250000,mode=run_mode)
    augment_and_train('wmh_e_withBackgrounds_SMonly_noSysts_lhe',observables,5,250000+3*250000,mode=run_mode)

