#!/bin/bash
#SBATCH -p lipq
#SBATCH --mem=4G

# Transfering the python script to the execution node
# INPUT = parton_level_analysis_met.py

# Loading Python environment
module load python/3.7.2

# Executing Python script
python parton_level_analysis_met.py

