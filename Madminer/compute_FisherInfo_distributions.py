# -*- coding: utf-8 -*-

"""
compute_FisherInfo_histograms.py

Computes Fisher Information distributions for signal and backgrounds.

Needs a trained SALLY model, since the calculation of the score depends on those.

Here, using only the 'met' set of observables (only observable degrees of freedom).

Ricardo Barrué (LIP/IST/CERN-ATLAS), 22/3/2021
"""

from __future__ import absolute_import, division, print_function, unicode_literals
import logging
import numpy as np
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt

# central object to extract the Fisher information from observables
from madminer.fisherinformation import FisherInformation
from madminer.fisherinformation import project_information, profile_information # for projecting/profiling over systematics
from madminer.plotting import plot_distribution_of_information

from pandas import DataFrame # to write the FI matrices

#from operator import 
# MadMiner output
logging.basicConfig(
    format='%(asctime)-5.5s %(name)-20.20s %(levelname)-7.7s %(message)s',
    datefmt='%H:%M',
    level=logging.INFO
)

# Output of all other modules (e.g. matplotlib)
for key in logging.Logger.manager.loggerDict:
    if "madminer" not in key:
        logging.getLogger(key).setLevel(logging.WARNING)

# Madgraph installation
#mg_dir = '/madminer/software/MG5_aMC_v2_6_7/' # local
mg_dir = '/cvmfs/sw.el7/gcc63/madgraph/2.7.3/b03/' # Pauli - only works after 'module load gcc63/madgraph/2.7.3'

# Partition to store h5 files and samples + path to setup file
main_proc_dir = '/lstore/calo/rbarrue/MadminerWHStudies/CPoddOnly_no_ang_obs' # Pauli
main_plot_dir = '/lstore/calo/rbarrue/CERNBox/work/HWWTensorStructureStudies/MadminerWHStudies/CPoddOnly_no_ang_obs'
setup_file='{}/setup_CPoddOnly.h5'.format(main_proc_dir)

# List of observables
observables='met'

# Integrated luminosity in fb^-1
lumi=300

# Labels for the axes of the matrix
labels=['cHW~']

# Function to plot the normalized distributions of observables and of the Fisher information
# signal_type can be "signalOnly", "withBackgrounds_SMonly" (SM signal + backgrounds) and "withBackgrounds" (SM signal + BSM signal + backgrounds)
def plot_distributions(observables,plot_observable,signal_type):

  # Histogram of cross-sections and information for each of the final state lepton charge and flavor combinations
  bin_boundaries, sigma_vals_wph_mu, fisher_infos_rate_wph_mu, fisher_infos_full_wph_mu = fisher_wph_mu.histogram_of_information(
    theta=[0.0],observable=plot_observable,
    nbins=plot_observable_info_dict[plot_observable][0],histrange=plot_observable_info_dict[plot_observable][1],
    model_file='{}/{}/models/sally_ensemble_wph_mu_{}_noSysts_lhe_{}'.format(main_proc_dir,observables,signal_type,observables),
    luminosity=lumi*1e3  
  )

  _, sigma_vals_wph_e, fisher_infos_rate_wph_e, fisher_infos_full_wph_e = fisher_wph_e.histogram_of_information(
    theta=[0.0],observable=plot_observable,
    nbins=plot_observable_info_dict[plot_observable][0],histrange=plot_observable_info_dict[plot_observable][1],
    model_file='{}/{}/models/sally_ensemble_wph_e_{}_noSysts_lhe_{}'.format(main_proc_dir,observables,signal_type,observables),
    luminosity=lumi*1e3  
  )

  _, sigma_vals_wmh_e, fisher_infos_rate_wmh_e, fisher_infos_full_wmh_e = fisher_wmh_e.histogram_of_information(
    theta=[0.0],observable=plot_observable,
    nbins=plot_observable_info_dict[plot_observable][0],histrange=plot_observable_info_dict[plot_observable][1],
    model_file='{}/{}/models/sally_ensemble_wmh_mu_{}_noSysts_lhe_{}'.format(main_proc_dir,observables,signal_type,observables),
    luminosity=lumi*1e3  
  )

  _, sigma_vals_wmh_e, fisher_infos_rate_wmh_e, fisher_infos_full_wmh_e = fisher_wmh_e.histogram_of_information(
    theta=[0.0],observable=plot_observable,
    nbins=plot_observable_info_dict[plot_observable][0],histrange=plot_observable_info_dict[plot_observable][1],
    model_file='{}/{}/models/sally_ensemble_wmh_e_{}_noSysts_lhe_{}'.format(main_proc_dir,observables,signal_type,observables),
    luminosity=lumi*1e3  
  )

  # Summing the cross-sections and the Fisher information matrices
  sigma_vals=[sum(x) for x in zip(sigma_vals_wph_mu,sigma_vals_wph_e,sigma_vals_wmh_e,sigma_vals_wmh_e)]

  fisher_infos_rate=fisher_infos_rate_wph_mu+fisher_infos_rate_wph_e+fisher_infos_rate_wmh_e+fisher_infos_rate_wmh_e

  fisher_infos_full=fisher_infos_full_wph_mu+fisher_infos_full_wph_e+fisher_infos_full_wmh_e+fisher_infos_full_wmh_e

  # Calculating the sum of Fisher information across bins (for normalization purposes)
  sigma_integral=sum(sigma_vals)
  fisher_infos_rate_integral=np.sum(fisher_infos_rate[:,0,0])
  fisher_infos_full_integral=np.sum(fisher_infos_full[:,0,0])

  # Writing the full Fisher information data to a CSV file for later access
  log_file=open('{}/{}/variable_information_{}.csv'.format(main_plot_dir,observables,signal_type),'a')
  #log_file.write('Observable name, total cross-sections, rate information, full information')
  log_file.write('{}, {}, {}, {} \n'.format(plot_observable, sigma_integral, fisher_infos_rate_integral, fisher_infos_full_integral)) # xs in pb

  # Distribution of information and cross-sections  
  fisher_info_xsec_histogram=plot_distribution_of_information(
    xbins=bin_boundaries,xsecs=sigma_vals,
    fisher_information_matrices=fisher_infos_full,
    fisher_information_matrices_aux=fisher_infos_rate,
    xlabel=plot_observable,
    xmin=plot_observable_info_dict[plot_observable][1][0],xmax=plot_observable_info_dict[plot_observable][1][1],
    figsize=(16.,9.),fontsize=24
  )

  # Saving the plot
  fisher_info_xsec_histogram.savefig('{}/{}/wh_{}_noSysts_lhe_{}.pdf'.format(main_plot_dir,observables,signal_type,plot_observable))

  log_file.close()
  

if __name__ == "__main__":

  observables='met'
  if observables=='met':
    from parton_level_analysis_met import observable_names
  else:
    exit('List of observables {} not defined. Exiting...'.format(observables))


  # Dictionary containing the information for the variable plotting: nbins,histrange
  plot_observable_info_dict=dict()

  for signal_type in ["signalOnly_SMonly","signalOnly", "withBackgrounds_SMonly","withBackgrounds"]:
    
    # Setting up the Fisher Information objects (loaded outside of function, since need to be done once per signal type and take some time to load)
    fisher_wph_mu=FisherInformation('{}/{}/wph_mu_{}_noSysts_lhe_{}.h5'.format(main_proc_dir,observables,signal_type,observables))
    fisher_wph_e=FisherInformation('{}/{}/wph_e_{}_noSysts_lhe_{}.h5'.format(main_proc_dir,observables,signal_type,observables))
    fisher_wmh_mu=FisherInformation('{}/{}/wmh_mu_{}_noSysts_lhe_{}.h5'.format(main_proc_dir,observables,signal_type,observables))
    fisher_wmh_e=FisherInformation('{}/{}/wmh_e_{}_noSysts_lhe_{}.h5'.format(main_proc_dir,observables,signal_type,observables))
    
    for plot_observable in observable_names:
      print('observable: {}'.format(plot_observable))
      if 'pt' in plot_observable:
        plot_observable_info_dict[plot_observable]=[20,(0.,800.)]
      elif 'mt' in plot_observable:
        plot_observable_info_dict[plot_observable]=[50,(0.,2000.)]
      elif 'dR' in plot_observable:
        plot_observable_info_dict[plot_observable]=[20,(0.,5.)]
      elif 'phi' in plot_observable:
        plot_observable_info_dict[plot_observable]=[20,(-3.2,3.2)]
      elif 'eta' in plot_observable and 'theta' not in plot_observable:
        plot_observable_info_dict[plot_observable]=[20,(-3.2,3.2)]
      elif 'met' in plot_observable:
        plot_observable_info_dict[plot_observable]=[20,(0.,800.)]
      else:
        print('Plot observable {} not in the set of relevant variables, skipping...'.format(plot_observable))
        continue
      
      plot_distributions(observables,plot_observable,signal_type)
    
  