# -*- coding: utf-8 -*-

"""
parton_level_analysis.py

Parton-level analysis of signal and background events using as observables only "physical" degrees of freedom (MET, mTW, q_l, etc.)

Includes a set of transfer functions (TF) to approximate detector response

Includes the calculation of the neutrino pZ in the same way as done in the ATLAS VH(bb) analyses

Includes the calculation of a set of angular observables.

Combines and shuffles the different samples
SM-only S+B sample is used in compute_FisherInfo_distributions.py for plotting purposes.

Ricardo Barrué (LIP/IST/CERN-ATLAS), 1/3/2021
"""

from __future__ import absolute_import, division, print_function, unicode_literals
import logging
import numpy as np
from math import cos, sqrt, fabs
import matplotlib
from matplotlib import pyplot as plt
import os

#import madminer
#from madminer import *
from madminer.core import MadMiner
from madminer.lhe import LHEReader
from madminer.sampling import combine_and_shuffle
from madminer.plotting import plot_distributions
from madminer.utils.particle import MadMinerParticle

# MadMiner output
logging.basicConfig(
    format='%(asctime)-5.5s %(name)-20.20s %(levelname)-7.7s %(message)s',
    datefmt='%H:%M',
    level=logging.INFO
)

# Output of all other modules (e.g. matplotlib)
for key in logging.Logger.manager.loggerDict:
    if "madminer" not in key:
        logging.getLogger(key).setLevel(logging.WARNING)

# Madgraph installation
#mg_dir = '/madminer/software/MG5_aMC_v2_6_7/' # local
mg_dir = '/cvmfs/sw.el7/gcc63/madgraph/2.7.3/b03/' # Pauli - only works after 'module load gcc63/madgraph/2.7.3'

# Partition to store h5 files and samples + path to setup file
main_proc_dir = '/lstore/calo/rbarrue/MadminerWHStudies/CPoddOnly_no_ang_obs' # Pauli
setup_file='{}/setup_CPoddOnly.h5'.format(main_proc_dir)

# "MET" set of observables
# Elements in the lists will be given as input to the add_observable function
observable_names = [
    'b1_px', 'b1_py', 'b1_pz', 'b1_e',
    'b2_px', 'b2_py', 'b2_pz', 'b2_e',
    'l_px', 'l_py', 'l_pz', 'l_e',
    'v_px', 'v_py',
    'pt_b1', 'pt_b2', 'pt_l', 'met', 'pt_w', 'pt_h',
    'eta_b1', 'eta_b2', 'eta_l', 'eta_h',
    'phi_b1', 'phi_b2', 'phi_l', 'phi_v', 'phi_w', 'phi_h',
    'theta_b1', 'theta_b2', 'theta_l', 'theta_h',
    'dphi_bb', 'dphi_lv', 'dphi_wh',
    'm_bb', 'mt_lv', 'mt_tot',
    'q_l',
    'dphi_lb1', 'dphi_lb2', 'dphi_vb1', 'dphi_vb2',
    'dR_bb', 'dR_lb1', 'dR_lb2'
]

list_of_observables = [
    'j[0].px', 'j[0].py', 'j[0].pz', 'j[0].e',
    'j[1].px', 'j[1].py', 'j[1].pz', 'j[1].e',
    'l[0].px', 'l[0].py', 'l[0].pz', 'l[0].e',
    'met.px', 'met.py',   
    'j[0].pt', 'j[1].pt', 'l[0].pt', 'met.pt', '(l[0] + met).pt', '(j[0] + j[1]).pt',
    'j[0].eta', 'j[1].eta', 'l[0].eta', '(j[0] + j[1]).eta',
    'j[0].phi()', 'j[1].phi()', 'l[0].phi()', 'met.phi()', '(l[0] + met).phi()', '(j[0] + j[1]).phi()',
    'j[0].theta()', 'j[1].theta()', 'l[0].theta()', '(j[0] + j[1]).theta()',
    'j[0].deltaphi(j[1])', 'l[0].deltaphi(met)', '(l[0] + met).deltaphi(j[0] + j[1])',
    '(j[0] + j[1]).m', '(l[0] + met).mt', '(j[0] + j[1] + l[0] + met).mt',
    'l[0].charge',
    'l[0].deltaphi(j[0])', 'l[0].deltaphi(j[1])', 'met.deltaphi(j[0])', 'met.deltaphi(j[1])',
    'j[0].deltar(j[1])', 'l[0].deltar(j[0])', 'l[0].deltar(j[1])'
]

# solves quadratic equation to get two possible solutions for pz_nu
# calculation similar to what is done in the ATLAS VHbb analyses (getNeutrinoPz function in AnalysisReader.cxx)
def get_neutrino_pz(particles=[],leptons=[],photons=[],jets=[],met=None,debug=False):

  # W boson mass (GeV)
  m_w=80.379
  
  pz_nu_1=0.0
  pz_nu_2=0.0

  if debug:
    print('get_neutrino_pz: leading lepton 4-vector {}'.format(leptons[0]))
    print('get_neutrino_pz: leading jet 4-vector {}'.format(jets[0]))
    print('get_neutrino_pz: subleading jet 4-vector {}'.format(jets[1]))
    print('get_neutrino_pz: met 4-vector {}'.format(met))

  # auxiliary quantities
  mu = 0.5 * pow(m_w,2) + (leptons[0].px*met.px + leptons[0].py*met.py) # = tmp/2
  delta = pow(mu*leptons[0].pz / pow(leptons[0].pt,2) ,2) - (pow(leptons[0].e,2)*pow(met.pt,2) - pow(mu,2))/pow(leptons[0].pt,2) 

  # neglecting any imaginary parts (equivalent to setting mtw = mw)
  if delta < 0.0:
    delta=0.0
  
  pz_nu_1=mu*leptons[0].pz/pow(leptons[0].pt,2) + sqrt(delta)
  pz_nu_2=mu*leptons[0].pz/pow(leptons[0].pt,2) - sqrt(delta)

  # NOTE: MadMinerParticle inherits from scikit-hep Lorentz vectors
  nu_1 = MadMinerParticle()
  nu_2 = MadMinerParticle()

  nu_1.setpxpypzm(met.px,met.py,pz_nu_1,0.0)
  nu_2.setpxpypzm(met.px,met.py,pz_nu_2,0.0)

  if debug:
    print('get_neutrino_pz: nu_1 4-vector: {}; nu_2 4-vector: {}'.format(nu_1,nu_2))

  h_candidate=jets[0]+jets[1]

  w_candidate_1=leptons[0]+nu_1
  w_candidate_2=leptons[0]+nu_2
  
  if debug:
    print('get_neutrino_pz: h_candidate 4-vector: {}'.format(h_candidate))
    print('get_neutrino_pz: w_candidate_1 4-vector: {}; w_candidate_2 4-vector: {}'.format(w_candidate_1,w_candidate_2))

    print('get_neutrino_pz: h_candidate_BetaZ: {}'.format(h_candidate.boostvector.z))

    print('get_neutrino_pz: w_candidate_1_BetaZ: {}'.format(w_candidate_1.boostvector.z))
    print('get_neutrino_pz: w_candidate_2_BetaZ: {}'.format(w_candidate_2.boostvector.z))
  

  dBetaZ_1=fabs(w_candidate_1.boostvector.z - h_candidate.boostvector.z)
  dBetaZ_2=fabs(w_candidate_2.boostvector.z - h_candidate.boostvector.z)

  if debug:
    print('get_neutrino_pz: dBetaZ_1: {}; dBetaZ_2: {}'.format(dBetaZ_1,dBetaZ_2))

  if dBetaZ_1 <= dBetaZ_2:
    if debug:
      print('get_neutrino_pz: pz_nu: {}'.format(pz_nu_1))
    return pz_nu_1
  else:
    if debug:
      print('get_neutrino_pz: pz_nu: {}'.format(pz_nu_2))
    return pz_nu_2
  #return pz_nu_1,pz_nu_2

def get_cos_thetaStar(particles=[],leptons=[],photons=[],jets=[],met=None,debug=False):

  pz_nu=get_neutrino_pz(leptons=leptons,jets=jets,met=met,debug=debug)

  nu=MadMinerParticle()
  nu.setpxpypzm(met.px,met.py,pz_nu,0.0)

  w_candidate=leptons[0]+nu
  lead_lepton_w_centerOfMass=leptons[0].boost(w_candidate.boostvector)


  if debug:

    print('get_cos_thetaStar: W candidate 4-vector: {}'.format(w_candidate))
    w_candidate_w_centerOfMass=w_candidate.boost(w_candidate.boostvector)
    print('get_cos_thetaStar: W candidate 4-vector boosted to the CM of the W candidate (expect[0.0,0.0,0.0,m_w]): {}'.format(w_candidate_w_centerOfMass))
    print('get_cos_thetaStar: leading lepton 4-vector boosted to the CM of the W candidate: {}'.format(lead_lepton_w_centerOfMass))
  

  cos_thetaStar=lead_lepton_w_centerOfMass.vector.dot(w_candidate.vector)/(fabs(lead_lepton_w_centerOfMass.p) * fabs(w_candidate.p))

  return cos_thetaStar

def get_cos_deltaPlus(particles=[],leptons=[],photons=[],jets=[],met=None,debug=False):
  
  pz_nu=get_neutrino_pz(leptons=leptons,jets=jets,met=met,debug=debug)

  nu=MadMinerParticle()
  nu.setpxpypzm(met.px,met.py,pz_nu,0.0)

  w_candidate=leptons[0]+nu
  lead_lepton_w_centerOfMass=leptons[0].boost(w_candidate.boostvector)

  if debug:
    print('get_cos_deltaPlus: W candidate 4-vector: {}'.format(w_candidate))
    w_candidate_w_centerOfMass=w_candidate.boost(w_candidate.boostvector)
    print('get_cos_deltaPlus: W candidate 4-vector boosted to the CM of the W candidate (expect[0.0,0.0,0.0,m_w]): {}'.format(w_candidate_w_centerOfMass))
    print('get_cos_deltaPlus: leading lepton 4-vector boosted to the CM of the W candidate: {}'.format(lead_lepton_w_centerOfMass))
  
  h_candidate=jets[0]+jets[1]

  h_cross_w=h_candidate.vector.cross(w_candidate.vector)

  cos_deltaPlus=lead_lepton_w_centerOfMass.vector.dot(h_cross_w)/(fabs(lead_lepton_w_centerOfMass.p) * fabs(h_cross_w.mag))

  return cos_deltaPlus

def get_cos_deltaMinus(particles=[],leptons=[],photons=[],jets=[],met=None,debug=False):

  pz_nu=get_neutrino_pz(leptons=leptons,jets=jets,met=met,debug=debug)

  nu=MadMinerParticle()
  nu.setpxpypzm(met.px,met.py,pz_nu,0.0)

  w_candidate=leptons[0]+nu

  h_candidate=jets[0]+jets[1]

  if debug:

    print('get_cos_deltaMinus: H candidate 4-vector: {}'.format(h_candidate))
    h_candidate_h_centerOfMass=h_candidate.boost(h_candidate.boostvector)
    print('get_cos_deltaMinus: H candidate 4-vector boosted to the CM of the H candidate (expect[0.0,0.0,0.0,m_H]): {}'.format(h_candidate_h_centerOfMass))
    
  lead_lepton_inv_h_centerOfMass=leptons[0].boost((-1)*h_candidate.boostvector)
  nu_inv_h_centerOfMass=nu.boost((-1)*h_candidate.boostvector)

  if debug:  
    print('get_cos_deltaMinus: leading lepton 4-vector boosted to the CM of the H candidate with an inverted momentum: {}'.format(lead_lepton_inv_h_centerOfMass))
    print('get_cos_deltaMinus: neutrino 4-vector boosted to the CM of the H candidate with an inverted momentum: {}'.format(nu_inv_h_centerOfMass))

  lep_cross_nu_inv_h_centerOfMass=lead_lepton_inv_h_centerOfMass.vector.cross(nu_inv_h_centerOfMass.vector)

  cos_deltaMinus=lep_cross_nu_inv_h_centerOfMass.dot(w_candidate.vector)/(fabs(lep_cross_nu_inv_h_centerOfMass.mag)*fabs(w_candidate.p))

  return cos_deltaMinus

def get_deltaphi_lv(particles=[],leptons=[],photons=[],jets=[],met=None,debug=False):
  
  pz_nu=get_neutrino_pz(leptons=leptons,jets=jets,met=met,debug=debug)

  nu=MadMinerParticle()
  nu.setpxpypzm(met.px,met.py,pz_nu,0.0)

  w_candidate=leptons[0]+nu
  lead_lepton_w_centerOfMass=leptons[0].boost(w_candidate.boostvector)

  if debug:

    print('get_cos_deltaPlus: W candidate 4-vector: {}'.format(w_candidate))
    w_candidate_w_centerOfMass=w_candidate.boost(w_candidate.boostvector)
    print('get_cos_deltaPlus: W candidate 4-vector boosted to the CM of the W candidate (expect[0.0,0.0,0.0,m_w]): {}'.format(w_candidate_w_centerOfMass))
    print('get_cos_deltaPlus: leading lepton 4-vector boosted to the CM of the W candidate: {}'.format(lead_lepton_w_centerOfMass))

  deltaphi_lv=lead_lepton_w_centerOfMass.deltaphi(w_candidate)

  return deltaphi_lv

# Function to process the events, run on each separate data sample
def process_events(event_path, setup_file_path,output_file_path,is_background_process=False,is_SM=True):

    # Load Madminer setup
    lhe = LHEReader(setup_file_path)
    
    # Smear b-quark energies using a (relative) Gaussian TF with width sigmaE/E = 0.1
    # Approximates the mBB distribution in CMS H(->bb) observation paper (1808.08242)
    lhe.set_smearing(
        pdgids=[5,-5],
        energy_resolution_abs=0,
        energy_resolution_rel=0.1,
        pt_resolution_abs=None, # can use this since I used massive b-quarks
        pt_resolution_rel=None  # calculating pt resolutions from an on-shell condition   
    )

    # Smear MET using a (absolute) Gaussian TF with sigmaMET = 12.5 GeV
    # Approximates the Run 2 ATLAS MET performance paper (1802.08168)
    lhe.set_met_noise(abs_=12.5, rel=0.0)

    # Add events
    if(is_SM):
        lhe.add_sample(
            '{}/Events/run_01/unweighted_events.lhe.gz'.format(event_path),
            sampled_from_benchmark='sm',
            is_background=is_background_process
        )
    else:
        list_BSM_benchmarks = [x for x in lhe.benchmark_names_phys if x != 'sm']
        for i,benchmark in enumerate(list_BSM_benchmarks,start=1):
            run_str = str(i)
            if len(run_str) < 2:
                run_str = '0' + run_str
            lhe.add_sample(
                '{}/Events/run_{}/unweighted_events.lhe.gz'.format(event_path, run_str),
                sampled_from_benchmark=benchmark,
                is_background=is_background_process
            )
    
    # Adding observables
    # "MET" observables    
    for i, name in enumerate(observable_names):
        lhe.add_observable( name, list_of_observables[i], required=True )
    
    #lhe.add_observable_from_function('pz_nu',get_neutrino_pz,required=True)
    #lhe.add_observable_from_function('cos_thetaStar',get_cos_thetaStar,required=True)
    #lhe.add_observable_from_function('cos_deltaPlus',get_cos_deltaPlus,required=True)
    #lhe.add_observable_from_function('cos_deltaMinus',get_cos_deltaMinus,required=True)
    #lhe.add_observable_from_function('deltaphi_lv',get_deltaphi_lv,required=True)

    # Emulates double b-tagging efficiency (flat 70% b-tagging probability for each)
    lhe.add_efficiency('0.7')
    lhe.add_efficiency('0.7')

    # Analyse samples and save the processed events as an .h5 file for later use
    lhe.analyse_samples()

    lhe.save(output_file_path)

if __name__ == "__main__":
  
  do_process=True
  do_combine=True
  do_backgrounds=True

  ############## Signal (W H -> l v b b ) ###############
  if do_process:

    os.makedirs('{}/met/signal/'.format(main_proc_dir),exist_ok=True)
    # W+ -> mu+ vm
    
    process_events(
        event_path='{}/signal_samples/wph_mu_smeftsim_SM'.format(main_proc_dir),
        setup_file_path=setup_file,
        is_background_process=False,
        is_SM=True,
        output_file_path='{}/met/signal/wph_mu_smeftsim_SM_lhe_met.h5'.format(main_proc_dir),
    )
    
    process_events(
        event_path='{}/signal_samples/wph_mu_smeftsim_BSM'.format(main_proc_dir),
        setup_file_path=setup_file,
        is_background_process=False,
        is_SM=False,
        output_file_path='{}/met/signal/wph_mu_smeftsim_BSM_lhe_met.h5'.format(main_proc_dir),
    )
      
    # W+ -> e+ ve
    process_events(
        event_path='{}/signal_samples/wph_e_smeftsim_SM'.format(main_proc_dir),
        setup_file_path=setup_file,
        is_background_process=False,
        is_SM=True,
        output_file_path='{}/met/signal/wph_e_smeftsim_SM_lhe_met.h5'.format(main_proc_dir),
    )

    process_events(
        event_path='{}/signal_samples/wph_e_smeftsim_BSM'.format(main_proc_dir),
        setup_file_path=setup_file,
        is_background_process=False,
        is_SM=False,
        output_file_path='{}/met/signal/wph_e_smeftsim_BSM_lhe_met.h5'.format(main_proc_dir),
    )

    # W- -> mu- vm~
    process_events(
        event_path='{}/signal_samples/wmh_mu_smeftsim_SM'.format(main_proc_dir),
        setup_file_path=setup_file,
        is_background_process=False,
        is_SM=True,
        output_file_path='{}/met/signal/wmh_mu_smeftsim_SM_lhe_met.h5'.format(main_proc_dir),
    )

    process_events(
        event_path='{}/signal_samples/wmh_mu_smeftsim_BSM'.format(main_proc_dir),
        setup_file_path=setup_file,
        is_background_process=False,
        is_SM=False,
        output_file_path='{}/met/signal/wmh_mu_smeftsim_BSM_lhe_met.h5'.format(main_proc_dir),
    )

    # W- -> e- ve~
    process_events(
        event_path='{}/signal_samples/wmh_e_smeftsim_SM'.format(main_proc_dir),
        setup_file_path=setup_file,
        is_background_process=False,
        is_SM=True,
        output_file_path='{}/met/signal/wmh_e_smeftsim_SM_lhe_met.h5'.format(main_proc_dir),
    )

    process_events(
        event_path='{}/signal_samples/wmh_e_smeftsim_BSM'.format(main_proc_dir),
        setup_file_path=setup_file,
        is_background_process=False,
        is_SM=False,
        output_file_path='{}/met/signal/wmh_e_smeftsim_BSM_lhe_met.h5'.format(main_proc_dir),
    )
    
    ############## Backgrounds ###############
    if do_backgrounds:
      
      os.makedirs('{}/met/background/'.format(main_proc_dir),exist_ok=True)
      ############## Single top tb -> W b b -> l v b b ###############
      # W+ -> mu+ vm
      process_events(
          event_path='{}/background_samples/tpb_mu_background'.format(main_proc_dir),
          setup_file_path=setup_file,
          is_background_process=True,
          is_SM=True,
          output_file_path='{}/met/background/tpb_mu_background_lhe_met.h5'.format(main_proc_dir),
      )

      # W+ -> e+ ve
      process_events(
          event_path='{}/background_samples/tpb_e_background'.format(main_proc_dir),
          setup_file_path=setup_file,
          is_background_process=True,
          is_SM=True,
          output_file_path='{}/met/background/tpb_e_background_lhe_met.h5'.format(main_proc_dir),
      )

      # W- -> mu- vm~
      process_events(
          event_path='{}/background_samples/tmb_mu_background'.format(main_proc_dir),
          setup_file_path=setup_file,
          is_background_process=True,
          is_SM=True,
          output_file_path='{}/met/background/tmb_mu_background_lhe_met.h5'.format(main_proc_dir),
      )

      # W- -> e- ve~
      process_events(
          event_path='{}/background_samples/tmb_e_background'.format(main_proc_dir),
          setup_file_path=setup_file,
          is_background_process=True,
          is_SM=True,
          output_file_path='{}/met/background/tmb_e_background_lhe_met.h5'.format(main_proc_dir),
      )


      ############## ttbar -> W b W b -> l v j j b b ###############

      # W+ -> mu+ vm
      process_events(
          event_path='{}/background_samples/tt_mupjj_background'.format(main_proc_dir),
          setup_file_path=setup_file,
          is_background_process=True,
          is_SM=True,
          output_file_path='{}/met/background/tt_mupjj_background_lhe_met.h5'.format(main_proc_dir),
      )

      # W+ -> e+ ve
      process_events(
          event_path='{}/background_samples/tt_epjj_background'.format(main_proc_dir),
          setup_file_path=setup_file,
          is_background_process=True,
          is_SM=True,
          output_file_path='{}/met/background/tt_epjj_background_lhe_met.h5'.format(main_proc_dir),
      )

      # W- -> mu- vm~
      process_events(
          event_path='{}/background_samples/tt_mumjj_background'.format(main_proc_dir),
          setup_file_path=setup_file,
          is_background_process=True,
          is_SM=True,
          output_file_path='{}/met/background/tt_mumjj_background_lhe_met.h5'.format(main_proc_dir),
      )

      # W- -> e- ve~
      process_events(
          event_path='{}/background_samples/tt_emjj_background'.format(main_proc_dir),
          setup_file_path=setup_file,
          is_background_process=True,
          is_SM=True,
          output_file_path='{}/met/background/tt_emjj_background_lhe_met.h5'.format(main_proc_dir),
      )


      ############## W + b-jets ###############

      # W+ -> mu+ vm
      process_events(
          event_path='{}/background_samples/wpbb_mu_background'.format(main_proc_dir),
          setup_file_path=setup_file,
          is_background_process=True,
          is_SM=True,
          output_file_path='{}/met/background/wpbb_mu_background_lhe_met.h5'.format(main_proc_dir),
      )

      # W+ -> e+ ve
      process_events(
          event_path='{}/background_samples/wpbb_e_background'.format(main_proc_dir),
          setup_file_path=setup_file,
          is_background_process=True,
          is_SM=True,
          output_file_path='{}/met/background/wpbb_e_background_lhe_met.h5'.format(main_proc_dir),
      )

      # W- -> mu- vm~
      process_events(
          event_path='{}/background_samples/wmbb_mu_background'.format(main_proc_dir),
          setup_file_path=setup_file,
          is_background_process=True,
          is_SM=True,
          output_file_path='{}/met/background/wmbb_mu_background_lhe_met.h5'.format(main_proc_dir),
      )

      # W- -> e- ve~
      process_events(
          event_path='{}/background_samples/wmbb_e_background'.format(main_proc_dir),
          setup_file_path=setup_file,
          is_background_process=True,
          is_SM=True,
          output_file_path='{}/met/background/wmbb_e_background_lhe_met.h5'.format(main_proc_dir),
      )
    
  if do_combine:

    # Combining and shuffling the signal samples
    # Combining and shuffling signal samples with background samples
    # Doing for each of the lepton flavors and charges

    # mu+ vm
    combine_and_shuffle([
      '{}/met/signal/wph_mu_smeftsim_SM_lhe_met.h5'.format(main_proc_dir),
      '{}/met/signal/wph_mu_smeftsim_BSM_lhe_met.h5'.format(main_proc_dir)],
      '{}/met/wph_mu_signalOnly_noSysts_lhe_met.h5'.format(main_proc_dir)
    )

    os.symlink('{}/met/signal/wph_mu_smeftsim_SM_lhe_met.h5'.format(main_proc_dir),'{}/met/wph_mu_signalOnly_SMonly_noSysts_lhe_met.h5'.format(main_proc_dir))

    if do_backgrounds:
      combine_and_shuffle([
        '{}/met/background/tpb_mu_background_lhe_met.h5'.format(main_proc_dir),
        '{}/met/background/tt_mupjj_background_lhe_met.h5'.format(main_proc_dir),
        '{}/met/background/wpbb_mu_background_lhe_met.h5'.format(main_proc_dir)],
        '{}/met/wph_mu_backgroundOnly_noSysts_lhe_met.h5'.format(main_proc_dir)
      )

      combine_and_shuffle([
        '{}/met/wph_mu_signalOnly_noSysts_lhe_met.h5'.format(main_proc_dir),
        '{}/met/wph_mu_backgroundOnly_noSysts_lhe_met.h5'.format(main_proc_dir)],
        '{}/met/wph_mu_withBackgrounds_noSysts_lhe_met.h5'.format(main_proc_dir)
      )
    
      combine_and_shuffle([
        '{}/met/signal/wph_mu_smeftsim_SM_lhe_met.h5'.format(main_proc_dir),
        '{}/met/wph_mu_backgroundOnly_noSysts_lhe_met.h5'.format(main_proc_dir)],
        '{}/met/wph_mu_withBackgrounds_SMonly_noSysts_lhe_met.h5'.format(main_proc_dir)
      )

    # e+ ve
    
    combine_and_shuffle([
      '{}/met/signal/wph_e_smeftsim_SM_lhe_met.h5'.format(main_proc_dir),
      '{}/met/signal/wph_e_smeftsim_BSM_lhe_met.h5'.format(main_proc_dir)],
      '{}/met/wph_e_signalOnly_noSysts_lhe_met.h5'.format(main_proc_dir)
    )

    os.symlink('{}/met/signal/wph_e_smeftsim_SM_lhe_met.h5'.format(main_proc_dir),'{}/met/wph_e_signalOnly_SMonly_noSysts_lhe_met.h5'.format(main_proc_dir))

    if do_backgrounds:
      combine_and_shuffle([
        '{}/met/background/tpb_e_background_lhe_met.h5'.format(main_proc_dir),
        '{}/met/background/tt_epjj_background_lhe_met.h5'.format(main_proc_dir),
        '{}/met/background/wpbb_e_background_lhe_met.h5'.format(main_proc_dir)],
        '{}/met/wph_e_backgroundOnly_noSysts_lhe_met.h5'.format(main_proc_dir)
      )

      combine_and_shuffle([
        '{}/met/wph_e_signalOnly_noSysts_lhe_met.h5'.format(main_proc_dir),
        '{}/met/wph_e_backgroundOnly_noSysts_lhe_met.h5'.format(main_proc_dir)],
        '{}/met/wph_e_withBackgrounds_noSysts_lhe_met.h5'.format(main_proc_dir)
      )
      
      combine_and_shuffle([
        '{}/met/signal/wph_e_smeftsim_SM_lhe_met.h5'.format(main_proc_dir),
        '{}/met/wph_e_backgroundOnly_noSysts_lhe_met.h5'.format(main_proc_dir)],
        '{}/met/wph_e_withBackgrounds_SMonly_noSysts_lhe_met.h5'.format(main_proc_dir)
      ) 

    # mu- vm~
    
    combine_and_shuffle([
      '{}/met/signal/wmh_mu_smeftsim_SM_lhe_met.h5'.format(main_proc_dir),
      '{}/met/signal/wmh_mu_smeftsim_BSM_lhe_met.h5'.format(main_proc_dir)],
      '{}/met/wmh_mu_signalOnly_noSysts_lhe_met.h5'.format(main_proc_dir)
    )

    os.symlink('{}/met/signal/wmh_mu_smeftsim_SM_lhe_met.h5'.format(main_proc_dir),'{}/met/wmh_mu_signalOnly_SMonly_noSysts_lhe_met.h5'.format(main_proc_dir))

    if do_backgrounds:
      combine_and_shuffle([
        '{}/met/background/tmb_mu_background_lhe_met.h5'.format(main_proc_dir),
        '{}/met/background/tt_mumjj_background_lhe_met.h5'.format(main_proc_dir),
        '{}/met/background/wmbb_mu_background_lhe_met.h5'.format(main_proc_dir)],
        '{}/met/wmh_mu_backgroundOnly_noSysts_lhe_met.h5'.format(main_proc_dir)
      )

      combine_and_shuffle([
        '{}/met/wmh_mu_signalOnly_noSysts_lhe_met.h5'.format(main_proc_dir),
        '{}/met/wmh_mu_backgroundOnly_noSysts_lhe_met.h5'.format(main_proc_dir)],
        '{}/met/wmh_mu_withBackgrounds_noSysts_lhe_met.h5'.format(main_proc_dir)
      )
      
      combine_and_shuffle([
        '{}/met/signal/wmh_mu_smeftsim_SM_lhe_met.h5'.format(main_proc_dir),
        '{}/met/wmh_mu_backgroundOnly_noSysts_lhe_met.h5'.format(main_proc_dir)],
        '{}/met/wmh_mu_withBackgrounds_SMonly_noSysts_lhe_met.h5'.format(main_proc_dir)
      )

    # e- ve~
    
    combine_and_shuffle([
      '{}/met/signal/wmh_e_smeftsim_SM_lhe_met.h5'.format(main_proc_dir),
      '{}/met/signal/wmh_e_smeftsim_BSM_lhe_met.h5'.format(main_proc_dir)],
      '{}/met/wmh_e_signalOnly_noSysts_lhe_met.h5'.format(main_proc_dir)
    )

    os.symlink('{}/met/signal/wmh_e_smeftsim_SM_lhe_met.h5'.format(main_proc_dir),'{}/met/wmh_e_signalOnly_SMonly_noSysts_lhe_met.h5'.format(main_proc_dir))

    if do_backgrounds:
      combine_and_shuffle([
        '{}/met/background/tmb_e_background_lhe_met.h5'.format(main_proc_dir),
        '{}/met/background/tt_emjj_background_lhe_met.h5'.format(main_proc_dir),
        '{}/met/background/wmbb_e_background_lhe_met.h5'.format(main_proc_dir)],
        '{}/met/wmh_e_backgroundOnly_noSysts_lhe_met.h5'.format(main_proc_dir)
      )

      combine_and_shuffle([
        '{}/met/wmh_e_signalOnly_noSysts_lhe_met.h5'.format(main_proc_dir),
        '{}/met/wmh_e_backgroundOnly_noSysts_lhe_met.h5'.format(main_proc_dir)],
        '{}/met/wmh_e_withBackgrounds_noSysts_lhe_met.h5'.format(main_proc_dir)
      )
    
      combine_and_shuffle([
        '{}/met/signal/wmh_e_smeftsim_SM_lhe_met.h5'.format(main_proc_dir),
        '{}/met/wmh_e_backgroundOnly_noSysts_lhe_met.h5'.format(main_proc_dir)],
        '{}/met/wmh_e_withBackgrounds_SMonly_noSysts_lhe_met.h5'.format(main_proc_dir)
      )    
