# -*- coding: utf-8 -*-

"""
compute_distributions.py

Computes distributions of different variables for signal and backgrounds.

Here, using only the 'met' set of observables (only observable degrees of freedom), where the angular observables are defined.

Ricardo Barrué (LIP/IST/CERN-ATLAS), 9/7/2021
"""

from __future__ import absolute_import, division, print_function, unicode_literals
import logging
import numpy as np
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt

# central object to extract the Fisher information from observables
from madminer.plotting import plot_distributions

#from operator import 
# MadMiner output
logging.basicConfig(
    format='%(asctime)-5.5s %(name)-20.20s %(levelname)-7.7s %(message)s',
    datefmt='%H:%M',
    level=logging.INFO
)

# Output of all other modules (e.g. matplotlib)
for key in logging.Logger.manager.loggerDict:
    if "madminer" not in key:
        logging.getLogger(key).setLevel(logging.WARNING)

# Partition to store h5 files and samples + path to setup file
main_proc_dir = '/lstore/calo/rbarrue/MadminerWHStudies/CPoddOnly_angObs_continuityTest/' # Pauli
main_plot_dir = '/lstore/calo/rbarrue/CERNBox/work/HWWTensorStructureStudies/MadminerWHStudies/CPoddOnly_angObs_continuityTest/'
setup_file='{}/setup_CPoddOnly_angObs_continuityTest.h5'.format(main_proc_dir)

if __name__ == '__main__':
  
  # W+ -> mu+ vm
  histo_wph_mu_signal=plot_distributions(filename='{}/met/wph_mu_signalOnly_noSysts_lhe_met.h5'.format(main_proc_dir),
  observables=['pz_nu','cos_thetaStar','cos_deltaPlus','cos_deltaMinus','deltaphi_lv'], normalize=True,n_bins=20,uncertainties='None',sample_only_from_closest_benchmark=False)

  histo_wph_mu_signal.savefig('{}/met/wph_mu_signalOnly_noSysts_lhe_met.pdf'.format(main_plot_dir))

  # W+ -> e+ ve
  histo_wph_e_signal=plot_distributions(filename='{}/met/wph_e_signalOnly_noSysts_lhe_met.h5'.format(main_proc_dir),
  observables=['pz_nu','cos_thetaStar','cos_deltaPlus','cos_deltaMinus','deltaphi_lv'], normalize=True,n_bins=20,uncertainties='None',sample_only_from_closest_benchmark=False)

  histo_wph_e_signal.savefig('{}/met/wph_e_signalOnly_noSysts_lhe_met.pdf'.format(main_plot_dir))

  # W- -> mu- vm~
  histo_wmh_mu_signal=plot_distributions(filename='{}/met/wmh_mu_signalOnly_noSysts_lhe_met.h5'.format(main_proc_dir),
  observables=['pz_nu','cos_thetaStar','cos_deltaPlus','cos_deltaMinus','deltaphi_lv'], normalize=True,n_bins=20,uncertainties='None',sample_only_from_closest_benchmark=False)

  histo_wmh_mu_signal.savefig('{}/met/wmh_mu_signalOnly_noSysts_lhe_met.pdf'.format(main_plot_dir))

  # W- -> e- ve~
  histo_wmh_e_signal=plot_distributions(filename='{}/met/wmh_e_signalOnly_noSysts_lhe_met.h5'.format(main_proc_dir),
  observables=['pz_nu','cos_thetaStar','cos_deltaPlus','cos_deltaMinus','deltaphi_lv'], normalize=True,n_bins=20,uncertainties='None',sample_only_from_closest_benchmark=False)

  histo_wmh_e_signal.savefig('{}/met/wmh_e_signalOnly_noSysts_lhe_met.pdf'.format(main_plot_dir))







 
