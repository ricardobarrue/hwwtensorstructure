# HWWTensorStructure/Madminer

Code developed for a study which uses Madminer to benchmark different observables and simulation-based inference techniques to constrain the EFT structure of the HWW vertex.

In LIP machines, for all steps except the generation step, Madminer can be accessed by loading Python 3.7.2 with
> module load python/3.7.2

To run the analysis chain the programs must be ran in a certain order:

1. _setup_CPoddOnly/redux/all.py_: defines the Wilson coefficient and morphing setup and creates setup file.

2. _gen_signal/background.py_: prepare the signal and background generation files to be ran on batch systems

**Note:** to run on LIP machines, one should first clear the Python3 environment and set up the Python2-based environment with:
> source setupMadminerGenerationPauli.sh 

This is because the latest version of SMEFTsim3 is crashing on LIP machines when using Python3 and the latest version of Madgraph. For all other steps, login again to clear the environment and return to the Python3 environment.

_launchAllGenJobsSLURM.sh_ is the shell script that sends out the generation jobs to the SLURM batch system.

3. _parton_level_analysis_full/met.py_: performs the parton-level analysis for the two different sets of observables

4. _sally_training.py_: train a multivariate method based on the score (SALLY - other methods may be added in the future)

5. _compute_FisherInfo_distributions.py_: calculates the Fisher information in several kinematic observables (to benchmark observables) and creates the differential distributions of the Fisher information in those observables (used to benchmark event selections/categorizations)

6. _compute_FisherInfo_histograms.py_: compute the Fisher information matrices in the complete truth-level information, the rate or 1D/2D histograms

7. _compute_FisherInfo_sally.py_: compute the Fisher information matrices obtained using the SALLY method

Step 4+5 and 6 can be run separately, although it makes sense to run them as 4+5->6 to first determine the best observables and then determine the limits obtained using different binnings.

Step 4+5 need to be ran in sequence since the calculation of the reco-level Fisher Information requires an estimator of the score.

