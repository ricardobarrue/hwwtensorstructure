# -*- coding: utf-8 -*-

"""
compute_FisherInfo_histograms.py

Computes Fisher Information matrices from the full kinematics using the score derived by the SALLY method.

Ricardo Barrué (LIP/IST/CERN-ATLAS), 22/3/2021
"""

from __future__ import absolute_import, division, print_function, unicode_literals
import logging
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import os

# central object to extract the Fisher information from observables
from madminer.fisherinformation import FisherInformation
from madminer.fisherinformation import project_information, profile_information # for projecting/profiling over systematics

from pandas import DataFrame # to write the FI matrices

# MadMiner output
logging.basicConfig(
    format='%(asctime)-5.5s %(name)-20.20s %(levelname)-7.7s %(message)s',
    datefmt='%H:%M',
    level=logging.INFO
)

# Output of all other modules (e.g. matplotlib)
for key in logging.Logger.manager.loggerDict:
    if "madminer" not in key:
        logging.getLogger(key).setLevel(logging.WARNING)

# Madgraph installation
#mg_dir = '/madminer/software/MG5_aMC_v2_6_7/' # local
mg_dir = '/cvmfs/sw.el7/gcc63/madgraph/2.7.3/b03/' # Pauli - only works after 'module load gcc63/madgraph/2.7.3'

# Partition to store h5 files and samples + path to setup file
main_proc_dir = '/lstore/calo/rbarrue/MadminerWHStudies/CPoddOnly_no_ang_obs' # Pauli
main_plot_dir = '/lstore/calo/rbarrue/CERNBox/work/HWWTensorStructureStudies/MadminerWHStudies/CPoddOnly_no_ang_obs' # folder with results
setup_file='{}/setup_CPoddOnly.h5'.format(main_proc_dir)

# Integrated luminosity in fb^-1
lumi=300

# Labels for the axes of the matrix
labels=['cHW~']

def get_FisherInfo_in_sally(observables='met',signal_type='signalOnly'):
  os.makedirs('{}/{}/fisher_info/'.format(main_proc_dir,observables),exist_ok=True)

  # Setting up the Fisher Information objects
  fisher_wph_mu=FisherInformation('{}/{}/wph_mu_{}_noSysts_lhe_{}.h5'.format(main_proc_dir,observables,signal_type,observables))
  fisher_wph_e=FisherInformation('{}/{}/wph_e_{}_noSysts_lhe_{}.h5'.format(main_proc_dir,observables,signal_type,observables))
  fisher_wmh_mu=FisherInformation('{}/{}/wmh_mu_{}_noSysts_lhe_{}.h5'.format(main_proc_dir,observables,signal_type,observables))
  fisher_wmh_e=FisherInformation('{}/{}/wmh_e_{}_noSysts_lhe_{}.h5'.format(main_proc_dir,observables,signal_type,observables))

  log_file=open('{}/fisherInfo_SALLY_{}_{}.txt'.format(main_plot_dir,signal_type,observables),'w')

  log_file.write('Observable list: {}; signal_type: {}'.format(observables,signal_type))

  fi_sally_wph_mu_mean, fi_sally_wph_mu_covariance=fisher_wph_mu.full_information(
    theta=[0.0],luminosity=lumi*1e3,
    model_file='{}/{}/models/sally_ensemble_wph_mu_{}_noSysts_lhe_{}'.format(main_proc_dir,observables,signal_type,observables)
  )

  fi_sally_wph_e_mean, fi_sally_wph_e_covariance=fisher_wph_e.full_information(
    theta=[0.0],luminosity=lumi*1e3,
    model_file='{}/{}/models/sally_ensemble_wph_e_{}_noSysts_lhe_{}'.format(main_proc_dir,observables,signal_type,observables)
  )

  fi_sally_wmh_mu_mean, fi_sally_wmh_mu_covariance=fisher_wmh_mu.full_information(
    theta=[0.0],luminosity=lumi*1e3,
    model_file='{}/{}/models/sally_ensemble_wmh_mu_{}_noSysts_lhe_{}'.format(main_proc_dir,observables,signal_type,observables)  
  )

  fi_sally_wmh_e_mean, fi_sally_wmh_e_covariance=fisher_wmh_e.full_information(
    theta=[0.0],luminosity=lumi*1e3,
    model_file='{}/{}/models/sally_ensemble_wmh_e_{}_noSysts_lhe_{}'.format(main_proc_dir,observables,signal_type,observables)    
  )

  df=DataFrame(
    data=fi_sally_wph_mu_mean+fi_sally_wph_e_mean+fi_sally_wmh_mu_mean+fi_sally_wmh_e_mean,
    columns=labels,
    index=labels)

  log_file.write( '\n SALLY information: \n'+df.to_string() )

  # Saving the Fisher information matrices for later use
  fi_sally_outfile = '{}/{}/fisher_info/fi_sally_{}.npz'.format(main_proc_dir,observables,signal_type)
  fi_sally_cov_outfile = '{}/{}/fisher_info/fi_sally_cov_{}.npz'.format(main_proc_dir,observables,signal_type)

  fi_sally_list = [
    fi_sally_wph_mu_mean, 
    fi_sally_wph_e_mean,
    fi_sally_wmh_mu_mean,
    fi_sally_wmh_e_mean 
  ]

  fi_sally_cov_list = [
    fi_sally_wph_mu_covariance, 
    fi_sally_wph_e_covariance,
    fi_sally_wmh_mu_covariance,
    fi_sally_wmh_e_covariance  
  ]

  np.savez(fi_sally_outfile, fi_sally_list, allow_pickle=False)
  np.savez(fi_sally_cov_outfile, fi_sally_cov_list, allow_pickle=False)

  log_file.close()

if __name__== "__main__":
  
  for signal_type in ['signalOnly','signalOnly_SMonly','withBackgrounds_SMonly','withBackgrounds']:
    for observables in ['full','met']:
      get_FisherInfo_in_sally(observables=observables,signal_type=signal_type)