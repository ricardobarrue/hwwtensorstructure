# -*- coding: utf-8 -*-

"""
compute_FisherInfo_histograms.py

Computes Fisher Information matrices from several quantities:

- complete truth-level information (as if one could measure the latent variables)

- rate (to see what is the sensitivity of the total rate)

- 1D or 2D histograms (starting from pTW ala VHbb STXS and moving to mTtot to angular observables)

Ricardo Barrué (LIP/IST/CERN-ATLAS), 19/3/2021
"""

from __future__ import absolute_import, division, print_function, unicode_literals
import logging
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import os

# central object to extract the Fisher information from observables
from madminer.fisherinformation import FisherInformation
from madminer.fisherinformation import project_information, profile_information # for projecting/profiling over systematics

from pandas import DataFrame # to write the FI matrices

#MadMiner output
logging.basicConfig(
    format='%(asctime)-5.5s %(name)-20.20s %(levelname)-7.7s %(message)s',
    datefmt='%H:%M',
    level=logging.INFO
)

# Output of all other modules (e.g. matplotlib)
for key in logging.Logger.manager.loggerDict:
    if "madminer" not in key:
      logging.getLogger(key).setLevel(logging.WARNING)

# Madgraph installation
#mg_dir = '/madminer/software/MG5_aMC_v2_6_7/' # local
mg_dir = '/cvmfs/sw.el7/gcc63/madgraph/2.7.3/b03/' # Pauli - only works after 'module load gcc63/madgraph/2.7.3'

# Partition to store h5 files and samples + path to setup file
main_proc_dir = '/lstore/calo/rbarrue/MadminerWHStudies/CPoddOnly_no_ang_obs' # Pauli
main_plot_dir = '/lstore/calo/rbarrue/CERNBox/work/HWWTensorStructureStudies/MadminerWHStudies/CPoddOnly_no_ang_obs'

setup_file='{}/setup_CPoddOnly.h5'.format(main_proc_dir)

# Integrated luminosity in fb^-1
lumi=300

# Labels for the axes of the matrix
labels=['cHW~']

def get_FisherInfo_in_histograms(observables='met',signal_type='signalOnly',do_truth=True,do_rate=True,do_simple_kinematic_observables=True,do_angular_observables=False):

  os.makedirs('{}/{}/fisher_info/'.format(main_proc_dir,observables),exist_ok=True)

  # Setting up the Fisher Information objects
  fisher_wph_mu=FisherInformation('{}/{}/wph_mu_{}_noSysts_lhe_{}.h5'.format(main_proc_dir,observables,signal_type,observables))
  fisher_wph_e=FisherInformation('{}/{}/wph_e_{}_noSysts_lhe_{}.h5'.format(main_proc_dir,observables,signal_type,observables))
  fisher_wmh_mu=FisherInformation('{}/{}/wmh_mu_{}_noSysts_lhe_{}.h5'.format(main_proc_dir,observables,signal_type,observables))
  fisher_wmh_e=FisherInformation('{}/{}/wmh_e_{}_noSysts_lhe_{}.h5'.format(main_proc_dir,observables,signal_type,observables))

  log_file=open('{}/fisherInfo_histograms_{}_{}.txt'.format(main_plot_dir,signal_type,observables),'a')

  log_file.write('\n Observable list: {}; signal_type: {} \n'.format(observables,signal_type))
  
  
  ############### Complete truth-level information (for signal-only samples) ##############

  if 'signalOnly' in signal_type and do_truth:

    fi_truth_wph_mu_mean, fi_truth_wph_mu_covariance=fisher_wph_mu.truth_information(
      theta=[0.0],luminosity=lumi*1e3,
    )

    fi_truth_wph_e_mean, fi_truth_wph_e_covariance=fisher_wph_e.truth_information(
      theta=[0.0],luminosity=lumi*1e3,
    )

    fi_truth_wmh_mu_mean, fi_truth_wmh_mu_covariance=fisher_wmh_mu.truth_information(
      theta=[0.0],luminosity=lumi*1e3,
    )

    fi_truth_wmh_e_mean, fi_truth_wmh_e_covariance=fisher_wmh_e.truth_information(
      theta=[0.0],luminosity=lumi*1e3,
    )

    df=DataFrame(
      data=fi_truth_wph_mu_mean+fi_truth_wph_e_mean+fi_truth_wmh_mu_mean+fi_truth_wmh_e_mean,
      columns=labels,
      index=labels)
    log_file.write( '\n Truth-level information: \n'+df.to_string())
    
    # Saving the Fisher information matrices for later use
    fi_truth_outfile = '{}/{}/fisher_info/fi_truth_{}.npz'.format(main_proc_dir,observables,signal_type)
    fi_truth_cov_outfile = '{}/{}/fisher_info/fi_truth_cov_{}.npz'.format(main_proc_dir,observables,signal_type)

    fi_truth_list = [
      fi_truth_wph_mu_mean, 
      fi_truth_wph_e_mean,
      fi_truth_wmh_mu_mean,
      fi_truth_wmh_e_mean 
    ]

    fi_truth_cov_list = [
      fi_truth_wph_mu_covariance, 
      fi_truth_wph_e_covariance,
      fi_truth_wmh_mu_covariance,
      fi_truth_wmh_e_covariance  
    ]

    np.savez(fi_truth_outfile, fi_truth_list, allow_pickle=False)
    np.savez(fi_truth_cov_outfile, fi_truth_cov_list, allow_pickle=False)

  ########## Rate information #########
  
  if do_rate:
    fi_rate_wph_mu_mean, fi_rate_wph_mu_covariance=fisher_wph_mu.rate_information(
      theta=[0.0],luminosity=lumi*1e3,
    )

    fi_rate_wph_e_mean, fi_rate_wph_e_covariance=fisher_wph_e.rate_information(
      theta=[0.0],luminosity=lumi*1e3,
    )

    fi_rate_wmh_mu_mean, fi_rate_wmh_mu_covariance=fisher_wmh_mu.rate_information(
      theta=[0.0],luminosity=lumi*1e3,
    )

    fi_rate_wmh_e_mean, fi_rate_wmh_e_covariance=fisher_wmh_e.rate_information(
      theta=[0.0],luminosity=lumi*1e3,
    )

    df=DataFrame(
      data=fi_rate_wph_mu_mean+fi_rate_wph_e_mean+fi_rate_wmh_mu_mean+fi_rate_wmh_e_mean,
      columns=labels,
      index=labels)
    log_file.write( '\n Rate information: \n'+df.to_string())
    
    
    fi_rate_outfile = '{}/{}/fisher_info/fi_rate_{}.npz'.format(main_proc_dir,observables,signal_type)
    fi_rate_cov_outfile = '{}/{}/fisher_info/fi_rate_{}_cov.npz'.format(main_proc_dir,observables,signal_type)

    fi_rate_list = [
      fi_rate_wph_mu_mean, 
      fi_rate_wph_e_mean,
      fi_rate_wmh_mu_mean,
      fi_rate_wmh_e_mean 
    ]

    fi_rate_cov_list = [
      fi_rate_wph_mu_covariance, 
      fi_rate_wph_e_covariance,
      fi_rate_wmh_mu_covariance,
      fi_rate_wmh_e_covariance  
    ]

    np.savez(fi_rate_outfile, fi_rate_list, allow_pickle=False)
    np.savez(fi_rate_cov_outfile, fi_rate_cov_list, allow_pickle=False)
  
  ########## Histogram information #########
  if do_simple_kinematic_observables:
    
    ########## pTW #########

    # Signal-only
    fi_ptw_wph_mu_mean, fi_ptw_wph_mu_covariance=fisher_wph_mu.histo_information(
      theta=[0.0],luminosity=lumi*1e3,
      observable='pt_w', bins=[0.,75.,150.,250.,400.,600.],
      histrange=(0.,600.)
    )

    fi_ptw_wph_e_mean, fi_ptw_wph_e_covariance=fisher_wph_e.histo_information(
      theta=[0.0],luminosity=lumi*1e3,
      observable='pt_w', bins=[0.,75.,150.,250.,400.,600.],
      histrange=(0.,600.)
    )

    fi_ptw_wmh_mu_mean, fi_ptw_wmh_mu_covariance=fisher_wmh_mu.histo_information(
      theta=[0.0],luminosity=lumi*1e3,
      observable='pt_w', bins=[0.,75.,150.,250.,400.,600.],
      histrange=(0.,600.)
    )

    fi_ptw_wmh_e_mean, fi_ptw_wmh_e_covariance=fisher_wmh_e.histo_information(
      theta=[0.0],luminosity=lumi*1e3,
      observable='pt_w', bins=[0.,75.,150.,250.,400.,600.],
      histrange=(0.,600.)
    )

    df=DataFrame(
      data=fi_ptw_wph_mu_mean+fi_ptw_wph_e_mean+fi_ptw_wmh_mu_mean+fi_ptw_wmh_e_mean,
      columns=labels,
      index=labels)
    log_file.write( '\n Information matrix for 6 bins of ptw [0-75,75-150,150-250,250-400,400-600,600-inf]: \n'+df.to_string())
    
    ########## pTW + mT_tot  #########

    # Signal-only
    fi_ptw_mttot_wph_mu_mean, fi_ptw_mttot_wph_mu_covariance=fisher_wph_mu.histo_information_2d(
      theta=[0.0],luminosity=lumi*1e3,
      observable1='pt_w', bins1=[0.,75.,150.,250.,400.],
      observable2='mt_tot',bins2=[0.,400.,800.],
      histrange1=(0.,400.),histrange2=(0.,800.)
    )

    fi_ptw_mttot_wph_e_mean, fi_ptw_mttot_wph_e_covariance=fisher_wph_e.histo_information_2d(
      theta=[0.0],luminosity=lumi*1e3,
      observable1='pt_w', bins1=[0.,75.,150.,250.,400.],
      observable2='mt_tot',bins2=[0.,400.,800.],
      histrange1=(0.,400.),histrange2=(0.,800.)
    )

    fi_ptw_mttot_wmh_mu_mean, fi_ptw_mttot_wmh_mu_covariance=fisher_wmh_mu.histo_information_2d(
      theta=[0.0],luminosity=lumi*1e3,
      observable1='pt_w', bins1=[0.,75.,150.,250.,400.],
      observable2='mt_tot',bins2=[0.,400.,800.],
      histrange1=(0.,400.),histrange2=(0.,800.)
    )

    fi_ptw_mttot_wmh_e_mean, fi_ptw_mttot_wmh_e_covariance=fisher_wmh_e.histo_information_2d(
      theta=[0.0],luminosity=lumi*1e3,
      observable1='pt_w', bins1=[0.,75.,150.,250.,400.],
      observable2='mt_tot',bins2=[0.,400.,800.],
      histrange1=(0.,400.),histrange2=(0.,800.)
    )

    df=DataFrame(
      data=fi_ptw_mttot_wph_mu_mean+fi_ptw_mttot_wph_e_mean+fi_ptw_mttot_wmh_mu_mean+fi_ptw_mttot_wmh_e_mean,
      columns=labels,
      index=labels)
    log_file.write( '\n Information matrix for ptw [0-75,75-150,150-250,250-400,400-inf] x mT_tot [0-400,400-800,800-inf]: \n'+df.to_string())
    
    ########## pTL  #########
    
    fi_ptl_wph_mu_mean, fi_ptl_wph_mu_covariance=fisher_wph_mu.histo_information(
      theta=[0.0],luminosity=lumi*1e3,
      observable='pt_l', bins=[0.,50.,100.,150.,200.],
      histrange=(0.,200.)
    )

    fi_ptl_wph_e_mean, fi_ptl_wph_e_covariance=fisher_wph_e.histo_information(
      theta=[0.0],luminosity=lumi*1e3,
      observable='pt_l', bins=[0.,50.,100.,150.,200.],
      histrange=(0.,200.)
    )

    fi_ptl_wmh_mu_mean, fi_ptl_wmh_mu_covariance=fisher_wmh_mu.histo_information(
      theta=[0.0],luminosity=lumi*1e3,
      observable='pt_l', bins=[0.,50.,100.,150.,200.],
      histrange=(0.,200.)
    )

    fi_ptl_wmh_e_mean, fi_ptl_wmh_e_covariance=fisher_wmh_e.histo_information(
      theta=[0.0],luminosity=lumi*1e3,
      observable='pt_l', bins=[0.,50.,100.,150.,200.],
      histrange=(0.,200.)
    )

    df=DataFrame(
      data=fi_ptl_wph_mu_mean+fi_ptl_wph_e_mean+fi_ptl_wmh_mu_mean+fi_ptl_wmh_e_mean,
      columns=labels,
      index=labels)
    log_file.write( '\n Information matrix for 5 bins of ptl [0-50,50-100,100-150,150-200,200-inf]: \n'+df.to_string())
    
    ########## pTL + mT_tot  #########
    
    # Signal-only
    fi_ptl_mttot_wph_mu_mean, fi_ptl_mttot_wph_mu_covariance=fisher_wph_mu.histo_information_2d(
      theta=[0.0],luminosity=lumi*1e3,
      observable1='pt_l', bins1=[0.,50.,100.,150.,200.],
      observable2='mt_tot',bins2=[0.,400.,800.],
      histrange1=(0.,200.),histrange2=(0.,800.)
    )

    fi_ptl_mttot_wph_e_mean, fi_ptl_mttot_wph_e_covariance=fisher_wph_e.histo_information_2d(
      theta=[0.0],luminosity=lumi*1e3,
      observable1='pt_l', bins1=[0.,50.,100.,150.,200.],
      observable2='mt_tot',bins2=[0.,400.,800.],
      histrange1=(0.,200.),histrange2=(0.,800.)
    )

    fi_ptl_mttot_wmh_mu_mean, fi_ptl_mttot_wmh_mu_covariance=fisher_wmh_mu.histo_information_2d(
      theta=[0.0],luminosity=lumi*1e3,
      observable1='pt_l', bins1=[0.,50.,100.,150.,200.],
      observable2='mt_tot',bins2=[0.,400.,800.],
      histrange1=(0.,200.),histrange2=(0.,800.)
    )

    fi_ptl_mttot_wmh_e_mean, fi_ptl_mttot_wmh_e_covariance=fisher_wmh_e.histo_information_2d(
      theta=[0.0],luminosity=lumi*1e3,
      observable1='pt_l', bins1=[0.,50.,100.,150.,200.],
      observable2='mt_tot',bins2=[0.,400.,800.],
      histrange1=(0.,200.),histrange2=(0.,800.)
    )

    df=DataFrame(
      data=fi_ptl_mttot_wph_mu_mean+fi_ptl_mttot_wph_e_mean+fi_ptl_mttot_wmh_mu_mean+fi_ptl_mttot_wmh_e_mean,
      columns=labels,
      index=labels)
    log_file.write( '\n Information matrix for ptl [0-50,50-100,100-150,150-200,200-inf] x mT_tot [0-400,400-800,800-inf]: \n'+df.to_string())

    


  log_file.close()

if __name__ == "__main__":

  for signal_type in ['signalOnly','signalOnly_SMonly','withBackgrounds_SMonly','withBackgrounds']:
    get_FisherInfo_in_histograms(observables='met',signal_type=signal_type,do_rate=False,do_truth=False)
    


