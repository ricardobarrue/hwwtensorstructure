# -*- coding: utf-8 -*-

"""
gen_signal.py

Generates WH signal events WH(->l v b b~), divided by W decay channel and charge

Can use different morphing setups (default: CP-odd operator only).

Using the SALLY method (around the SM) -> generating a larger number of events for that point.

Generating a smaller number of events for all the BSM benchmarks (generates extra SM events).

Ricardo Barrué (LIP/IST/CERN-ATLAS), 19/2/2021
"""


from __future__ import absolute_import, division, print_function, unicode_literals
import logging
import numpy as np
import matplotlib
import os

from madminer.core import MadMiner
from madminer.lhe import LHEReader


# MadMiner output
logging.basicConfig(
    format='%(asctime)-5.5s %(name)-20.20s %(levelname)-7.7s %(message)s',
    datefmt='%H:%M',
    level=logging.INFO
)

# Output of all other modules (e.g. matplotlib)
for key in logging.Logger.manager.loggerDict:
    if "madminer" not in key:
        logging.getLogger(key).setLevel(logging.WARNING)

# Madgraph installation
#mg_dir = '/madminer/software/MG5_aMC_v2_6_7/' # local
mg_dir = '/cvmfs/sw.el7/gcc63/madgraph/2.7.3/b03/' # Pauli - only works after 'module load gcc63/madgraph/2.7.3'

# Partition to store h5 files and samples + path to setup file
main_proc_dir = '/lstore/calo/rbarrue/MadminerWHStudies/CPoddOnly' # Pauli

if __name__ == "__main__":

  # Load morphing setup file
  miner = MadMiner()
  miner.load('{}/setup_CPoddOnly.h5'.format(main_proc_dir))
  lhe = LHEReader('{}/setup_CPoddOnly.h5'.format(main_proc_dir))

  # List of BSM benchmarks - SM + 1 BSM benchmarks (from Madminer)
  list_BSM_benchmarks = [x for x in lhe.benchmark_names_phys if x != 'sm']

  # W+ -> mu+ vm
  # SM benchmarks
  miner.run(
      mg_directory=mg_dir,
      log_directory='{}/logs/wph_mu_smeftsim_SM'.format(main_proc_dir),
      mg_process_directory='{}/signal_samples/wph_mu_smeftsim_SM'.format(main_proc_dir),
      proc_card_file='cards/signal_processes/proc_card_wph_mu_smeftsim.dat',
      param_card_template_file='cards/param_card_template_SMEFTsim3_MwScheme.dat',
      #pythia8_card_file='cards/pythia8_card.dat',
      sample_benchmark='sm',
      run_card_file='cards/run_card_250k_WHMadminerCuts.dat',
      initial_command='module load gcc63/madgraph/2.7.3 ',
      only_prepare_script=True
  )

  # BSM benchmarks
  miner.run_multiple(
      mg_directory=mg_dir,
      log_directory='{}/logs/wph_mu_smeftsim_BSM'.format(main_proc_dir),
      mg_process_directory='{}/signal_samples/wph_mu_smeftsim_BSM'.format(main_proc_dir),
      proc_card_file='cards/signal_processes/proc_card_wph_mu_smeftsim.dat',
      param_card_template_file='cards/param_card_template_SMEFTsim3_MwScheme_autoWidths.dat',
      #pythia8_card_file='cards/pythia8_card.dat',
      sample_benchmarks=list_BSM_benchmarks,
      run_card_files=['cards/run_card_50k_WHMadminerCuts.dat'],
      initial_command='module load gcc63/madgraph/2.7.3',
      only_prepare_script=True
  )

  # W+ -> e+ ve
  miner.run(
      mg_directory=mg_dir,
      log_directory='{}/logs/wph_e_smeftsim_SM'.format(main_proc_dir),
      mg_process_directory='{}/signal_samples/wph_e_smeftsim_SM'.format(main_proc_dir),
      proc_card_file='cards/signal_processes/proc_card_wph_e_smeftsim.dat',
      param_card_template_file='cards/param_card_template_SMEFTsim3_MwScheme.dat',
      #pythia8_card_file='cards/pythia8_card.dat',
      sample_benchmark='sm',
      run_card_file='cards/run_card_250k_WHMadminerCuts.dat',
      initial_command='module load gcc63/madgraph/2.7.3',
      only_prepare_script=True
  )

  miner.run_multiple(
      mg_directory=mg_dir,
      log_directory='{}/logs/wph_e_smeftsim_BSM'.format(main_proc_dir),
      mg_process_directory='{}/signal_samples/wph_e_smeftsim_BSM'.format(main_proc_dir),
      proc_card_file='cards/signal_processes/proc_card_wph_e_smeftsim.dat',
      param_card_template_file='cards/param_card_template_SMEFTsim3_MwScheme_autoWidths.dat',
      #pythia8_card_file='cards/pythia8_card.dat',
      sample_benchmarks=list_BSM_benchmarks,
      run_card_files=['cards/run_card_50k_WHMadminerCuts.dat'],
      initial_command='module load gcc63/madgraph/2.7.3',
      only_prepare_script=True
  )

  # W- -> mu- vm~
  miner.run(
      mg_directory=mg_dir,
      log_directory='{}/logs/wmh_mu_smeftsim_SM'.format(main_proc_dir),
      mg_process_directory='{}/signal_samples/wmh_mu_smeftsim_SM'.format(main_proc_dir),
      proc_card_file='cards/signal_processes/proc_card_wmh_mu_smeftsim.dat',
      param_card_template_file='cards/param_card_template_SMEFTsim3_MwScheme.dat',
      #pythia8_card_file='cards/pythia8_card.dat',
      sample_benchmark='sm',
      run_card_file='cards/run_card_250k_WHMadminerCuts.dat',
      initial_command='module load gcc63/madgraph/2.7.3',
      only_prepare_script=True
  )

  miner.run_multiple(
      mg_directory=mg_dir,
      log_directory='{}/logs/wmh_mu_smeftsim_BSM'.format(main_proc_dir),
      mg_process_directory='{}/signal_samples/wmh_mu_smeftsim_BSM'.format(main_proc_dir),
      proc_card_file='cards/signal_processes/proc_card_wmh_mu_smeftsim.dat',
      param_card_template_file='cards/param_card_template_SMEFTsim3_MwScheme_autoWidths.dat',
      #pythia8_card_file='cards/pythia8_card.dat',
      sample_benchmarks=list_BSM_benchmarks,
      run_card_files=['cards/run_card_50k_WHMadminerCuts.dat'],
      initial_command='module load gcc63/madgraph/2.7.3',
      only_prepare_script=True
  )

  # W- -> e- ve~
  miner.run(
      mg_directory=mg_dir,
      log_directory='{}/logs/wmh_e_smeftsim_SM'.format(main_proc_dir),
      mg_process_directory='{}/signal_samples/wmh_e_smeftsim_SM'.format(main_proc_dir),
      proc_card_file='cards/signal_processes/proc_card_wmh_e_smeftsim.dat',
      param_card_template_file='cards/param_card_template_SMEFTsim3_MwScheme.dat',
      #pythia8_card_file='cards/pythia8_card.dat',
      sample_benchmark='sm',
      run_card_file='cards/run_card_250k_WHMadminerCuts.dat',
      initial_command='module load gcc63/madgraph/2.7.3',
      only_prepare_script=True
  )

  miner.run_multiple(
      mg_directory=mg_dir,
      log_directory='{}/logs/wmh_e_smeftsim_BSM'.format(main_proc_dir),
      mg_process_directory='{}/signal_samples/wmh_e_smeftsim_BSM'.format(main_proc_dir),
      proc_card_file='cards/signal_processes/proc_card_wmh_e_smeftsim.dat',
      param_card_template_file='cards/param_card_template_SMEFTsim3_MwScheme_autoWidths.dat',
      #pythia8_card_file='cards/pythia8_card.dat',
      sample_benchmarks=list_BSM_benchmarks,
      run_card_files=['cards/run_card_50k_WHMadminerCuts.dat'],
      initial_command='module load gcc63/madgraph/2.7.3',
      only_prepare_script=True
  )

  os.remove('/tmp/generate.mg5')

  
