# -*- coding: utf-8 -*-

"""
Madminer parameter and morphing setup code for a WH signal.

Includes only the CP-odd operator (oHWtil).

Ricardo Barrue (LIP/IST/CERN-ATLAS), 15/2/2020
"""

from __future__ import absolute_import, division, print_function, unicode_literals

import os
import logging
import numpy as np
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt

# MadMiner output
logging.basicConfig(
    format='%(asctime)-5.5s %(name)-20.20s %(levelname)-7.7s %(message)s',
    datefmt='%H:%M',
    level=logging.INFO
)

# Output of all other modules (e.g. matplotlib)
for key in logging.Logger.manager.loggerDict:
    if "madminer" not in key:
        logging.getLogger(key).setLevel(logging.WARNING)

from madminer import MadMiner

# Partition to store h5 files and samples + path to setup file
main_proc_dir = '/lstore/calo/rbarrue/MadminerWHStudies/CPoddOnly' # Pauli

if __name__ == "__main__":

  # Instance of MadMiner core class
  miner = MadMiner()

  miner.add_parameter(
      lha_block='smeftcpv',
      lha_id=4,
      parameter_name='cHWtil',
      morphing_max_power=1, # interference only
      parameter_range=(-1.0,1.0)
  )

  # Only want the SM benchmark specifically - let Madminer choose the others
  miner.add_benchmark({'cHWtil':0.000000},'sm')


  # Morphing - automatic optimization to avoid large weights
  miner.set_morphing(max_overall_power=1,include_existing_benchmarks=True)

  miner.save('{}/setup_CPoddOnly.h5'.format(main_proc_dir))
