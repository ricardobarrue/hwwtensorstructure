# -*- coding: utf-8 -*-

"""
parton_level_analysis_full.py

Parton-level analysis of signal and background events with a complete set of observables (including leading neutrino 4-vector).

Includes a set of transfer functions (TF) to approximate detector response.

In the end combines and shuffles the signal and bkg samples for each lepton flavor and charge.

Ricardo Barrué (LIP/IST/CERN-ATLAS), 1/3/2021
"""

from __future__ import absolute_import, division, print_function, unicode_literals
import logging
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import os, shutil

from madminer.core import MadMiner
from madminer.lhe import LHEReader
from madminer.sampling import combine_and_shuffle

# MadMiner output
logging.basicConfig(
    format='%(asctime)-5.5s %(name)-20.20s %(levelname)-7.7s %(message)s',
    datefmt='%H:%M',
    level=logging.INFO
)

# Output of all other modules (e.g. matplotlib)
for key in logging.Logger.manager.loggerDict:
    if "madminer" not in key:
        logging.getLogger(key).setLevel(logging.WARNING)

# Madgraph installation
#mg_dir = '/madminer/software/MG5_aMC_v2_6_7/' # local
mg_dir = '/cvmfs/sw.el7/gcc63/madgraph/2.7.3/b03/' # Pauli - only works after 'module load gcc63/madgraph/2.7.3'

# Partition to store h5 files and samples + path to setup file
main_proc_dir = '/lstore/calo/rbarrue/MadminerWHStudies/CPoddOnly_no_ang_obs' # Pauli
setup_file='{}/setup_CPoddOnly.h5'.format(main_proc_dir)

# "Full" set of observables
# Elements in the lists will be given as input to the add_observable function
observable_names = [
    'b1_px', 'b1_py', 'b1_pz', 'b1_e',
    'b2_px', 'b2_py', 'b2_pz', 'b2_e',
    'l_px', 'l_py', 'l_pz', 'l_e',
    'v_px', 'v_py', 'v_pz', 'v_e',
    'pt_b1', 'pt_b2', 'pt_l1', 'pt_l2', 'pt_w', 'pt_h',
    'eta_b1', 'eta_b2', 'eta_l', 'eta_v', 'eta_w', 'eta_h',
    'phi_b1', 'phi_b2', 'phi_l', 'phi_v', 'phi_w', 'phi_h',
    'theta_b1', 'theta_b2', 'theta_l', 'theta_v', 'theta_w', 'theta_h',
    'dphi_bb', 'dphi_lv', 'dphi_wh',
    'm_bb', 'm_lv', 'm_tot',
    'q_l', 'q_v', 'q_b1', 'q_b2',
    'dphi_lb1', 'dphi_lb2', 'dphi_vb1', 'dphi_vb2',
    'dR_bb', 'dR_lv', 'dR_lb1', 'dR_lb2', 'dR_vb1', 'dR_vb2'
]

list_of_observables = [
    'j[0].px', 'j[0].py', 'j[0].pz', 'j[0].e',
    'j[1].px', 'j[1].py', 'j[1].pz', 'j[1].e',
    'l[0].px', 'l[0].py', 'l[0].pz', 'l[0].e',
    'v[0].px', 'v[0].py', 'v[0].pz', 'v[0].e',
    'j[0].pt', 'j[1].pt', 'l[0].pt', 'v[0].pt', '(l[0] + v[0]).pt', '(j[0] + j[1]).pt',
    'j[0].eta', 'j[1].eta', 'l[0].eta', 'v[0].eta', '(l[0] + v[0]).eta', '(j[0] + j[1]).eta',
    'j[0].phi()', 'j[1].phi()', 'l[0].phi()', 'v[0].phi()', '(l[0] + v[0]).phi()', '(j[0] + j[1]).phi()',
    'j[0].theta()', 'j[1].theta()', 'l[0].theta()', 'v[0].theta()', '(l[0] + v[0]).theta()', '(j[0] + j[1]).theta()',
    'j[0].deltaphi(j[1])', 'l[0].deltaphi(v[0])', '(l[0] + v[0]).deltaphi(j[0] + j[1])',
    '(j[0] + j[1]).m', '(l[0] + v[0]).m', '(j[0] + j[1] + l[0] + v[0]).m',
    'l[0].charge', 'v[0].charge', 'j[0].charge', 'j[1].charge',
    'l[0].deltaphi(j[0])', 'l[0].deltaphi(j[1])', 'v[0].deltaphi(j[0])', 'v[0].deltaphi(j[1])',
    'j[0].deltar(j[1])', 'l[0].deltar(v[0])', 'l[0].deltar(j[0])', 'l[0].deltar(j[1])', 'v[0].deltar(j[0])', 'v[0].deltar(j[1])',
]

# Function to process the events, run on each separate data sample
def process_events(event_path, setup_file_path,output_file_path,is_background_process=False,is_SM=True):

    # Load Madminer setup
    lhe = LHEReader(setup_file_path)

    # Smear b-quark energies using a (relative) Gaussian TF with width sigmaE/E = 0.1
    # Approximates the mBB distribution in CMS H(->bb) observation paper (1808.08242)
    lhe.set_smearing(
        pdgids=[5,-5],
        energy_resolution_abs=0,
        energy_resolution_rel=0.1,
        pt_resolution_abs=None, # can use this since I used massive b-quarks
        pt_resolution_rel=None  # calculating pt resolutions from an on-shell condition
    )

    # Smear MET using a (absolute) Gaussian TF with sigmaMET = 12.5 GeV
    # Approximates the Run 2 ATLAS MET performance paper (1802.08168)

    # For the "full" set of observables, smearing the neutrino momenta
    lhe.set_smearing(
        pdgids=[12,-12,14,-14,16,-16],
        energy_resolution_abs=12.5,
        energy_resolution_rel=0.0,
        pt_resolution_abs=None,
        pt_resolution_rel=None
    )

    # Add events
    if(is_SM):
        lhe.add_sample(
            '{}/Events/run_01/unweighted_events.lhe.gz'.format(event_path),
            sampled_from_benchmark='sm',
            is_background=is_background_process
        )
    else:
        list_BSM_benchmarks = [x for x in lhe.benchmark_names_phys if x != 'sm']
        for i,benchmark in enumerate(list_BSM_benchmarks,start=1):
            run_str = str(i)
            if len(run_str) < 2:
                run_str = '0' + run_str
            lhe.add_sample(
                '{}/Events/run_{}/unweighted_events.lhe.gz'.format(event_path, run_str),
                sampled_from_benchmark=benchmark,
                is_background=is_background_process
            )

    # Adding observables
    for i, name in enumerate(observable_names):
        lhe.add_observable( name, list_of_observables[i], required=True )

    # Emulates double b-tagging efficiency (flat 70% b-tagging probability for each)
    lhe.add_efficiency('0.7')
    lhe.add_efficiency('0.7')

    # Analyse samples and save the processed events as an .h5 file for later use
    lhe.analyse_samples()

    lhe.save(output_file_path)

if __name__ == "__main__":

  do_process=False
  do_combine=True
  do_backgrounds=True

  ############## Signal (W H -> l v b b ) ###############
  if do_process:

    os.makedirs('{}/full/signal/'.format(main_proc_dir),exist_ok=True)

    # W+ -> mu+ vm
    process_events(
        event_path='{}/signal_samples/wph_mu_smeftsim_SM'.format(main_proc_dir),
        setup_file_path=setup_file,
        is_background_process=False,
        is_SM=True,
        output_file_path='{}/full/signal/wph_mu_smeftsim_SM_lhe_full.h5'.format(main_proc_dir),
    )

    process_events(
        event_path='{}/signal_samples/wph_mu_smeftsim_BSM'.format(main_proc_dir),
        setup_file_path=setup_file,
        is_background_process=False,
        is_SM=False,
        output_file_path='{}/full/signal/wph_mu_smeftsim_BSM_lhe_full.h5'.format(main_proc_dir),
    )

    # W+ -> e+ ve
    process_events(
        event_path='{}/signal_samples/wph_e_smeftsim_SM'.format(main_proc_dir),
        setup_file_path=setup_file,
        is_background_process=False,
        is_SM=True,
        output_file_path='{}/full/signal/wph_e_smeftsim_SM_lhe_full.h5'.format(main_proc_dir),
    )

    process_events(
        event_path='{}/signal_samples/wph_e_smeftsim_BSM'.format(main_proc_dir),
        setup_file_path=setup_file,
        is_background_process=False,
        is_SM=False,
        output_file_path='{}/full/signal/wph_e_smeftsim_BSM_lhe_full.h5'.format(main_proc_dir),
    )

    # W- -> mu- vm~
    process_events(
        event_path='{}/signal_samples/wmh_mu_smeftsim_SM'.format(main_proc_dir),
        setup_file_path=setup_file,
        is_background_process=False,
        is_SM=True,
        output_file_path='{}/full/signal/wmh_mu_smeftsim_SM_lhe_full.h5'.format(main_proc_dir),
    )

    process_events(
        event_path='{}/signal_samples/wmh_mu_smeftsim_BSM'.format(main_proc_dir),
        setup_file_path=setup_file,
        is_background_process=False,
        is_SM=False,
        output_file_path='{}/full/signal/wmh_mu_smeftsim_BSM_lhe_full.h5'.format(main_proc_dir),
    )

    # W- -> e- ve~
    process_events(
        event_path='{}/signal_samples/wmh_e_smeftsim_SM'.format(main_proc_dir),
        setup_file_path=setup_file,
        is_background_process=False,
        is_SM=True,
        output_file_path='{}/full/signal/wmh_e_smeftsim_SM_lhe_full.h5'.format(main_proc_dir),
    )

    process_events(
        event_path='{}/signal_samples/wmh_e_smeftsim_BSM'.format(main_proc_dir),
        setup_file_path=setup_file,
        is_background_process=False,
        is_SM=False,
        output_file_path='{}/full/signal/wmh_e_smeftsim_BSM_lhe_full.h5'.format(main_proc_dir),
    )

    ############## Backgrounds ###############
    do_backgrounds=False

    if do_backgrounds:

      os.makedirs('{}/full/background/'.format(main_proc_dir),exist_ok=True)

      ############## Single top tb -> W b b -> l v b b ###############

      # W+ -> mu+ vm
      process_events(
          event_path='{}/background_samples/tpb_mu_background'.format(main_proc_dir),
          setup_file_path=setup_file,
          is_background_process=True,
          is_SM=True,
          output_file_path='{}/full/background/tpb_mu_background_lhe_full.h5'.format(main_proc_dir),
      )

      # W+ -> e+ ve
      process_events(
          event_path='{}/background_samples/tpb_e_background'.format(main_proc_dir),
          setup_file_path=setup_file,
          is_background_process=True,
          is_SM=True,
          output_file_path='{}/full/background/tpb_e_background_lhe_full.h5'.format(main_proc_dir),
      )

      # W- -> mu- vm~
      process_events(
          event_path='{}/background_samples/tmb_mu_background'.format(main_proc_dir),
          setup_file_path=setup_file,
          is_background_process=True,
          is_SM=True,
          output_file_path='{}/full/background/tmb_mu_background_lhe_full.h5'.format(main_proc_dir),
      )

      # W- -> e- ve~
      process_events(
          event_path='{}/background_samples/tmb_e_background'.format(main_proc_dir),
          setup_file_path=setup_file,
          is_background_process=True,
          is_SM=True,
          output_file_path='{}/full/background/tmb_e_background_lhe_full.h5'.format(main_proc_dir),
      )


      ############## ttbar -> W b W b -> l v j j b b ###############

      # W+ -> mu+ vm
      process_events(
          event_path='{}/background_samples/tt_mupjj_background'.format(main_proc_dir),
          setup_file_path=setup_file,
          is_background_process=True,
          is_SM=True,
          output_file_path='{}/full/background/tt_mupjj_background_lhe_full.h5'.format(main_proc_dir),
      )

      # W+ -> e+ ve
      process_events(
          event_path='{}/background_samples/tt_epjj_background'.format(main_proc_dir),
          setup_file_path=setup_file,
          is_background_process=True,
          is_SM=True,
          output_file_path='{}/full/background/tt_epjj_background_lhe_full.h5'.format(main_proc_dir),
      )

      # W- -> mu- vm~
      process_events(
          event_path='{}/background_samples/tt_mumjj_background'.format(main_proc_dir),
          setup_file_path=setup_file,
          is_background_process=True,
          is_SM=True,
          output_file_path='{}/full/background/tt_mumjj_background_lhe_full.h5'.format(main_proc_dir),
      )

      # W- -> e- ve~
      process_events(
          event_path='{}/background_samples/tt_emjj_background'.format(main_proc_dir),
          setup_file_path=setup_file,
          is_background_process=True,
          is_SM=True,
          output_file_path='{}/full/background/tt_emjj_background_lhe_full.h5'.format(main_proc_dir),
      )


      ############## W + b-jets ###############

      # W+ -> mu+ vm
      process_events(
          event_path='{}/background_samples/wpbb_mu_background'.format(main_proc_dir),
          setup_file_path=setup_file,
          is_background_process=True,
          is_SM=True,
          output_file_path='{}/full/background/wpbb_mu_background_lhe_full.h5'.format(main_proc_dir),
      )

      # W+ -> e+ ve
      process_events(
          event_path='{}/background_samples/wpbb_e_background'.format(main_proc_dir),
          setup_file_path=setup_file,
          is_background_process=True,
          is_SM=True,
          output_file_path='{}/full/background/wpbb_e_background_lhe_full.h5'.format(main_proc_dir),
      )

      # W- -> mu- vm~
      process_events(
          event_path='{}/background_samples/wmbb_mu_background'.format(main_proc_dir),
          setup_file_path=setup_file,
          is_background_process=True,
          is_SM=True,
          output_file_path='{}/full/background/wmbb_mu_background_lhe_full.h5'.format(main_proc_dir),
      )

      # W- -> e- ve~
      process_events(
          event_path='{}/background_samples/wmbb_e_background'.format(main_proc_dir),
          setup_file_path=setup_file,
          is_background_process=True,
          is_SM=True,
          output_file_path='{}/full/background/wmbb_e_background_lhe_full.h5'.format(main_proc_dir),
      )

  if do_combine:

    # Combining and shuffling the signal samples
    # Combining and shuffling the background samples
    # Combining and shuffling signal samples with background samples
    # Doing for each of the lepton flavors and charges

    # mu+ vm
    combine_and_shuffle([
      '{}/full/signal/wph_mu_smeftsim_SM_lhe_full.h5'.format(main_proc_dir),
      '{}/full/signal/wph_mu_smeftsim_BSM_lhe_full.h5'.format(main_proc_dir)],
      '{}/full/wph_mu_signalOnly_noSysts_lhe_full.h5'.format(main_proc_dir)
    )

    os.symlink('{}/full/signal/wph_mu_smeftsim_SM_lhe_full.h5'.format(main_proc_dir),'{}/full/wph_mu_signalOnly_SMonly_noSysts_lhe_full.h5'.format(main_proc_dir))

    if do_backgrounds:

      combine_and_shuffle([
        '{}/full/background/tpb_mu_background_lhe_full.h5'.format(main_proc_dir),
        '{}/full/background/tt_mupjj_background_lhe_full.h5'.format(main_proc_dir),
        '{}/full/background/wpbb_mu_background_lhe_full.h5'.format(main_proc_dir)],
        '{}/full/wph_mu_backgroundOnly_noSysts_lhe_full.h5'.format(main_proc_dir)
      )

      combine_and_shuffle([
        '{}/full/wph_mu_signalOnly_noSysts_lhe_full.h5'.format(main_proc_dir),
        '{}/full/wph_mu_backgroundOnly_noSysts_lhe_full.h5'.format(main_proc_dir)],
        '{}/full/wph_mu_withBackgrounds_noSysts_lhe_full.h5'.format(main_proc_dir)
      )

      combine_and_shuffle([
        '{}/full/signal/wph_mu_smeftsim_SM_lhe_full.h5'.format(main_proc_dir),
        '{}/full/wph_mu_backgroundOnly_noSysts_lhe_full.h5'.format(main_proc_dir)],
        '{}/full/wph_mu_withBackgrounds_SMonly_noSysts_lhe_full.h5'.format(main_proc_dir)
      )      

    # e+ ve
    combine_and_shuffle([
      '{}/full/signal/wph_e_smeftsim_SM_lhe_full.h5'.format(main_proc_dir),
      '{}/full/signal/wph_e_smeftsim_BSM_lhe_full.h5'.format(main_proc_dir)],
      '{}/full/wph_e_signalOnly_noSysts_lhe_full.h5'.format(main_proc_dir)
    )

    os.symlink('{}/full/signal/wph_e_smeftsim_SM_lhe_full.h5'.format(main_proc_dir),'{}/full/wph_e_signalOnly_SMonly_noSysts_lhe_full.h5'.format(main_proc_dir))

    if do_backgrounds:

      combine_and_shuffle([
        '{}/full/background/tpb_e_background_lhe_full.h5'.format(main_proc_dir),
        '{}/full/background/tt_epjj_background_lhe_full.h5'.format(main_proc_dir),
        '{}/full/background/wpbb_e_background_lhe_full.h5'.format(main_proc_dir)],
        '{}/full/wph_e_backgroundOnly_noSysts_lhe_full.h5'.format(main_proc_dir)
      )

      combine_and_shuffle([
        '{}/full/wph_e_signalOnly_noSysts_lhe_full.h5'.format(main_proc_dir),
        '{}/full/wph_e_backgroundOnly_noSysts_lhe_full.h5'.format(main_proc_dir)],
        '{}/full/wph_e_withBackgrounds_noSysts_lhe_full.h5'.format(main_proc_dir)
      )

      combine_and_shuffle([
        '{}/full/signal/wph_e_smeftsim_SM_lhe_full.h5'.format(main_proc_dir),
        '{}/full/wph_e_backgroundOnly_noSysts_lhe_full.h5'.format(main_proc_dir)],
        '{}/full/wph_e_withBackgrounds_SMonly_noSysts_lhe_full.h5'.format(main_proc_dir)
      ) 

    # mu- vm~
    combine_and_shuffle([
      '{}/full/signal/wmh_mu_smeftsim_SM_lhe_full.h5'.format(main_proc_dir),
      '{}/full/signal/wmh_mu_smeftsim_BSM_lhe_full.h5'.format(main_proc_dir)],
      '{}/full/wmh_mu_signalOnly_noSysts_lhe_full.h5'.format(main_proc_dir)
    )
    
    os.symlink('{}/full/signal/wmh_mu_smeftsim_SM_lhe_full.h5'.format(main_proc_dir),'{}/full/wmh_mu_signalOnly_SMonly_noSysts_lhe_full.h5'.format(main_proc_dir))

    if do_backgrounds:
      combine_and_shuffle([
        '{}/full/background/tmb_mu_background_lhe_full.h5'.format(main_proc_dir),
        '{}/full/background/tt_mumjj_background_lhe_full.h5'.format(main_proc_dir),
        '{}/full/background/wmbb_mu_background_lhe_full.h5'.format(main_proc_dir)],
        '{}/full/wmh_mu_backgroundOnly_noSysts_lhe_full.h5'.format(main_proc_dir)
      )

      combine_and_shuffle([
        '{}/full/wmh_mu_signalOnly_noSysts_lhe_full.h5'.format(main_proc_dir),
        '{}/full/wmh_mu_backgroundOnly_noSysts_lhe_full.h5'.format(main_proc_dir)],
        '{}/full/wmh_mu_withBackgrounds_noSysts_lhe_full.h5'.format(main_proc_dir)
      )

      combine_and_shuffle([
        '{}/full/signal/wmh_mu_smeftsim_SM_lhe_full.h5'.format(main_proc_dir),
        '{}/full/wmh_mu_backgroundOnly_noSysts_lhe_full.h5'.format(main_proc_dir)],
        '{}/full/wmh_mu_withBackgrounds_SMonly_noSysts_lhe_full.h5'.format(main_proc_dir)
      )      

    # e- ve~
    combine_and_shuffle([
      '{}/full/signal/wmh_e_smeftsim_SM_lhe_full.h5'.format(main_proc_dir),
      '{}/full/signal/wmh_e_smeftsim_BSM_lhe_full.h5'.format(main_proc_dir)],
      '{}/full/wmh_e_signalOnly_noSysts_lhe_full.h5'.format(main_proc_dir)
    )

    os.symlink('{}/full/signal/wmh_e_smeftsim_SM_lhe_full.h5'.format(main_proc_dir),'{}/full/wmh_e_signalOnly_SMonly_noSysts_lhe_full.h5'.format(main_proc_dir))

    if do_backgrounds:

      combine_and_shuffle([
        '{}/full/background/tmb_e_background_lhe_full.h5'.format(main_proc_dir),
        '{}/full/background/tt_emjj_background_lhe_full.h5'.format(main_proc_dir),
        '{}/full/background/wmbb_e_background_lhe_full.h5'.format(main_proc_dir)],
        '{}/full/wmh_e_backgroundOnly_noSysts_lhe_full.h5'.format(main_proc_dir)
      )

      combine_and_shuffle([
        '{}/full/wmh_e_signalOnly_noSysts_lhe_full.h5'.format(main_proc_dir),
        '{}/full/wmh_e_backgroundOnly_noSysts_lhe_full.h5'.format(main_proc_dir)],
        '{}/full/wmh_e_withBackgrounds_noSysts_lhe_full.h5'.format(main_proc_dir)
      )

      combine_and_shuffle([
        '{}/full/signal/wmh_e_smeftsim_SM_lhe_full.h5'.format(main_proc_dir),
        '{}/full/wmh_e_backgroundOnly_noSysts_lhe_full.h5'.format(main_proc_dir)],
        '{}/full/wmh_e_withBackgrounds_SMonly_noSysts_lhe_full.h5'.format(main_proc_dir)
      )  
