# -*- coding: utf-8 -*-

"""
gen_background.py

Generates background events: W + b b~, t t~, single top t b
- divided by W decay channel and charge

Ricardo Barrué (LIP/IST/CERN-ATLAS), 19/2/2021
"""

from __future__ import absolute_import, division, print_function, unicode_literals
import logging
import numpy as np
import matplotlib
from matplotlib import pyplot as plt

from madminer.core import MadMiner
from madminer.lhe import LHEReader


# MadMiner output
logging.basicConfig(
    format='%(asctime)-5.5s %(name)-20.20s %(levelname)-7.7s %(message)s',
    datefmt='%H:%M',
    level=logging.INFO
)

# Output of all other modules (e.g. matplotlib)
for key in logging.Logger.manager.loggerDict:
    if "madminer" not in key:
        logging.getLogger(key).setLevel(logging.WARNING)

# Madgraph installation
#mg_dir = '/madminer/software/MG5_aMC_v2_6_7/' # local
mg_dir = '/cvmfs/sw.el7/gcc63/madgraph/2.7.3/b03/' # Pauli - only works after 'module load gcc63/madgraph/2.7.3'

# Partion to store scripts and samples container
main_proc_dir = '/lstore/calo/rbarrue/MadminerWHStudies/CPoddOnly' # Pauli

if __name__ == "__main__":

  # Load morphing setup file - this doesn't matter since we will use the Madgraph 'sm' model
  miner = MadMiner()
  miner.load('{}/setup_CPoddOnly.h5'.format(main_proc_dir))
  lhe = LHEReader('{}/setup_CPoddOnly.h5'.format(main_proc_dir))

  # W + b b~, divided by W decay channel and charge

  # W+ -> mu+ vm 
  miner.run(
      mg_directory=mg_dir,
      log_directory='{}/logs/wpbb_mu_background'.format(main_proc_dir),
      mg_process_directory='{}/background_samples/wpbb_mu_background'.format(main_proc_dir),
      proc_card_file='cards/background_processes/proc_card_wpbb_mu.dat',
      param_card_template_file='cards/param_card_template_SMEFTsim3_MwScheme.dat',
      #pythia8_card_file='cards/pythia8_card.dat',
      run_card_file='cards/run_card_250k_WHMadminerCuts.dat',
      sample_benchmark='sm',
      initial_command='module load gcc63/madgraph/2.7.3',
      is_background=True,
      only_prepare_script=True
  )

  # W+ -> e+ ve 
  miner.run(
      mg_directory=mg_dir,
      log_directory='{}/logs/wpbb_e_background'.format(main_proc_dir),
      mg_process_directory='{}/background_samples/wpbb_e_background'.format(main_proc_dir),
      proc_card_file='cards/background_processes/proc_card_wpbb_e.dat',
      param_card_template_file='cards/param_card_template_SMEFTsim3_MwScheme.dat',
      #pythia8_card_file='cards/pythia8_card.dat',
      run_card_file='cards/run_card_250k_WHMadminerCuts.dat',
      sample_benchmark='sm',
      initial_command='module load gcc63/madgraph/2.7.3',
      is_background=True,
      only_prepare_script=True
  )

  # W- -> mu- vm~
  miner.run(
      mg_directory=mg_dir,
      log_directory='{}/logs/wmbb_mu_background'.format(main_proc_dir),
      mg_process_directory='{}/background_samples/wmbb_mu_background'.format(main_proc_dir),
      proc_card_file='cards/background_processes/proc_card_wmbb_mu.dat',
      param_card_template_file='cards/param_card_template_SMEFTsim3_MwScheme.dat',
      #pythia8_card_file='cards/pythia8_card.dat',
      run_card_file='cards/run_card_250k_WHMadminerCuts.dat',
      sample_benchmark='sm',
      initial_command='module load gcc63/madgraph/2.7.3',
      is_background=True,
      only_prepare_script=True
  )

  # W- -> e- ve~
  miner.run(
      mg_directory=mg_dir,
      log_directory='{}/logs/wmbb_e_background'.format(main_proc_dir),
      mg_process_directory='{}/background_samples/wmbb_e_background'.format(main_proc_dir),
      proc_card_file='cards/background_processes/proc_card_wmbb_e.dat',
      param_card_template_file='cards/param_card_template_SMEFTsim3_MwScheme.dat',
      #pythia8_card_file='cards/pythia8_card.dat',
      run_card_file='cards/run_card_250k_WHMadminerCuts.dat',
      sample_benchmark='sm',
      initial_command='module load gcc63/madgraph/2.7.3',
      is_background=True,
      only_prepare_script=True
  )

  # tb production, divided by top (W) charge and W decay channel

  # t+, W+ -> mu+ vm 

  miner.run(
      mg_directory=mg_dir,
      log_directory='{}/logs/tpb_mu_background'.format(main_proc_dir),
      mg_process_directory='{}/background_samples/tpb_mu_background'.format(main_proc_dir),
      proc_card_file='cards/background_processes/proc_card_tpb_mu.dat',
      param_card_template_file='cards/param_card_template_SMEFTsim3_MwScheme.dat',
      #pythia8_card_file='cards/pythia8_card.dat',
      run_card_file='cards/run_card_250k_WHMadminerCuts.dat',
      sample_benchmark='sm',
      initial_command='module load gcc63/madgraph/2.7.3',
      is_background=True,
      only_prepare_script=True
  )

  # t+, W+ -> e+ ve 
  miner.run(
      mg_directory=mg_dir,
      log_directory='{}/logs/tpb_e_background'.format(main_proc_dir),
      mg_process_directory='{}/background_samples/tpb_e_background'.format(main_proc_dir),
      proc_card_file='cards/background_processes/proc_card_tpb_e.dat',
      param_card_template_file='cards/param_card_template_SMEFTsim3_MwScheme.dat',
      #pythia8_card_file='cards/pythia8_card.dat',
      run_card_file='cards/run_card_250k_WHMadminerCuts.dat',
      sample_benchmark='sm',
      initial_command='module load gcc63/madgraph/2.7.3',
      is_background=True,
      only_prepare_script=True
  )

  # t-, W- -> mu- vm~ 
  miner.run(
      mg_directory=mg_dir,
      log_directory='{}/logs/tmb_mu_background'.format(main_proc_dir),
      mg_process_directory='{}/background_samples/tmb_mu_background'.format(main_proc_dir),
      proc_card_file='cards/background_processes/proc_card_tmb_mu.dat',
      param_card_template_file='cards/param_card_template_SMEFTsim3_MwScheme.dat',
      #pythia8_card_file='cards/pythia8_card.dat',
      run_card_file='cards/run_card_250k_WHMadminerCuts.dat',
      sample_benchmark='sm',
      initial_command='module load gcc63/madgraph/2.7.3',
      is_background=True,
      only_prepare_script=True
  )

  # t-, W- -> e- ve~
  miner.run(
      mg_directory=mg_dir,
      log_directory='{}/logs/tmb_e_background'.format(main_proc_dir),
      mg_process_directory='{}/background_samples/tmb_e_background'.format(main_proc_dir),
      proc_card_file='cards/background_processes/proc_card_tmb_e.dat',
      param_card_template_file='cards/param_card_template_SMEFTsim3_MwScheme.dat',
      #pythia8_card_file='cards/pythia8_card.dat',
      run_card_file='cards/run_card_250k_WHMadminerCuts.dat',
      sample_benchmark='sm',
      initial_command='module load gcc63/madgraph/2.7.3',
      is_background=True,
      only_prepare_script=True
  )

  # semi-leptonic ttbar production, divided in possible decay channels

  # W+ -> mu+ vm, W- -> j j 
  miner.run(
      mg_directory=mg_dir,
      log_directory='{}/logs/tt_mupjj_background'.format(main_proc_dir),
      mg_process_directory='{}/background_samples/tt_mupjj_background'.format(main_proc_dir),
      proc_card_file='cards/background_processes/proc_card_tt_mupjj.dat',
      param_card_template_file='cards/param_card_template_SMEFTsim3_MwScheme.dat',
      #pythia8_card_file='cards/pythia8_card.dat',
      run_card_file='cards/run_card_250k_WHMadminerCuts.dat',
      sample_benchmark='sm',
      initial_command='module load gcc63/madgraph/2.7.3',
      is_background=True,
      only_prepare_script=True
  )

  # W+ -> e+ ve, W- -> j j 
  miner.run(
      mg_directory=mg_dir,
      log_directory='{}/logs/tt_epjj_background'.format(main_proc_dir),
      mg_process_directory='{}/background_samples/tt_epjj_background'.format(main_proc_dir),
      proc_card_file='cards/background_processes/proc_card_tt_epjj.dat',
      param_card_template_file='cards/param_card_template_SMEFTsim3_MwScheme.dat',
      #pythia8_card_file='cards/pythia8_card.dat',
      run_card_file='cards/run_card_250k_WHMadminerCuts.dat',
      sample_benchmark='sm',
      initial_command='module load gcc63/madgraph/2.7.3',
      is_background=True,
      only_prepare_script=True
  )

  # W+ -> j j, W- -> mu- vm~ 
  miner.run(
      mg_directory=mg_dir,
      log_directory='{}/logs/tt_mumjj_background'.format(main_proc_dir),
      mg_process_directory='{}/background_samples/tt_mumjj_background'.format(main_proc_dir),
      proc_card_file='cards/background_processes/proc_card_tt_mumjj.dat',
      param_card_template_file='cards/param_card_template_SMEFTsim3_MwScheme.dat',
      #pythia8_card_file='cards/pythia8_card.dat',
      run_card_file='cards/run_card_250k_WHMadminerCuts.dat',
      sample_benchmark='sm',
      initial_command='module load gcc63/madgraph/2.7.3',
      is_background=True,
      only_prepare_script=True
  )

  # W+ -> j j, W- -> e- ve~
  miner.run(
      mg_directory=mg_dir,
      log_directory='{}/logs/tt_emjj_background'.format(main_proc_dir),
      mg_process_directory='{}/background_samples/tt_emjj_background'.format(main_proc_dir),
      proc_card_file='cards/background_processes/proc_card_tt_emjj.dat',
      param_card_template_file='cards/param_card_template_SMEFTsim3_MwScheme.dat',
      #pythia8_card_file='cards/pythia8_card.dat',
      run_card_file='cards/run_card_250k_WHMadminerCuts.dat',
      sample_benchmark='sm',
      initial_command='module load gcc63/madgraph/2.7.3',
      is_background=True,
      only_prepare_script=True
  )
