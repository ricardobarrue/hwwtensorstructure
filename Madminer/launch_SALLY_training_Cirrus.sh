#!/bin/bash

#SBATCH --partition=gpu
#SBATCH --gres=gpu
#SBATCH --mem=16384MB
#SBATCH --ntasks=1
#SBATCH --profile=task

module purge
#module load python/3.7.2
#module load cuda
module use /cvmfs/sw.el7/modules/hpc.test
module load tensorflow # RB: instalação "all in one"

nvcc --version
nvidia-smi
python -m torch.utils.collect_env

CUDA_LAUNCH_BLOCKING=1 python /users3/t3atlas/rbarrue/HWWTensorStructureCode/Madminer/sally_training.py
