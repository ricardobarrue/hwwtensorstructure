#!/bin/bash
#SBATCH -p lipq
#SBATCH --mem=6G

# Transfering the python script to the execution node
# INPUT = Tuplizer.C

# Loading Python environment
module purge
module load gcc63/root/6.18.04 gcc63/delphes/3.4.1

# Executing Python script
root Tuplizer.C

