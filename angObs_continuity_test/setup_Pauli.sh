# !/bin/bash

# loads the necessary modules
module purge
module load gcc63/root/6.18.04 gcc63/delphes/3.4.1 

# exports the location of Buzatu Python to the PYTHONPATH
export PYTHONPATH=${PWD}/../BuzatuPython/:$PYTHONPATH
