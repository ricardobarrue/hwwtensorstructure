# -*- coding: utf-8 -*-

from ROOT import gROOT, TFile, TTree, TH1F
from ROOT import kBlue, kRed, kBlack, kGreen, kMagenta
from ROOT import kSolid, kDotted, kDashDotted
import sys
from HelperPyRoot import overlayHistograms

# dictionary connecting the value of the couplings with the specific folder number where the events are stored

couplings_label_dir_dict={
  'SM'       : 'run_01/',
  #'cHW~=-1.0': 'run_01/',
  #'cHW~=-0.8': 'run_02/',
  #'cHW~=-0.6': 'run_03/',
  #'cHW~=-0.4': 'run_04/',
  #'cHW~=-0.2': 'run_05/',
  'cHW~=0.2' : 'run_06/',
  'cHW~=0.4' : 'run_07/',
  'cHW~=0.6' : 'run_08/',
  'cHW~=0.8' : 'run_09/',
  #'cHW~=1.0' : 'run_10/',
}

# plot information for the different angular observables of the form [nbins,xmin,xmax]
observable_plotInfo_dict={
  'cosTStar':[12,-1.2,1.2]
}

main_proc_dir='/lstore/calo/rbarrue/MadminerWHStudies/CPoddOnly_angObs_continuityTest/signal_samples/'

#output_dir='/lstore/calo/rbarrue/CERNBox/work/HWWTensorStructureStudies/CPoddOnly_angObs_continuity_test/'
output_dir='/lstore/calo/rbarrue/CERNBox/work/HWWTensorStructureStudies/CPoddOnly_angObs_coupling_sign_test/'

colors_lineStyles_list=[(kBlue,kSolid),
                        (kRed,kDotted),
                        (kBlack,kDashDotted),
                        (kGreen,5),
                        (kMagenta,6)]

def plotter(input_path='',channel='wmh_e_smeftsim',observable='cosTStar',file_name='GEN_Delphes.root',do_ratio_pad=False,plot_label='Angular observable continuity test',do_weighted=True,do_shape_only=False,legend_info=[0.575,0.75,0.725,0.85,72,0.028,0],output_path='./'):

  gROOT.SetBatch(True)

  if input_path=='':
    sys.exit('plotter: input_path not defined, exiting.')

  if output_path=='./':
    sys.exit('plotter: no output path defined, saving plot to where script is being ran')
  
  if len(couplings_label_dir_dict) > len(colors_lineStyles_list):
    sys.exit('plotter: you have more plots to overlay than color/linestyle options. Confirm options then run script again')

  # create the required histograms with a predefined binning 
  nbins=observable_plotInfo_dict[observable][0]
  xmin=observable_plotInfo_dict[observable][1]
  xmax=observable_plotInfo_dict[observable][2]
  bin_width=round(float((xmax-xmin)/nbins),3)

  list_histograms_to_compare=[]

  for i_label,label in enumerate(couplings_label_dir_dict):

    if label=='SM':
      input_folder_suffix='SM'
    else:
      input_folder_suffix='BSM'

    h=TH1F('h_{}_{}'.format(observable,i_label),';{};Events/{}'.format(observable,bin_width),nbins,xmin,xmax)

    # fetching the file with the tree with the different observables
    f=TFile('{}/{}_{}/Events/{}/{}'.format(input_path,channel,input_folder_suffix,couplings_label_dir_dict[label],file_name))
    
    t=f.Get('Nominal')

    # Filling histograms
    for event in t:
      obs_value=getattr(event,observable)
      event_weight=getattr(event,'EventWeight')

      if do_weighted:
        h.Fill(obs_value,event_weight)
      else:
        h.Fill(obs_value)
    
    if do_shape_only:
      h.Scale(1/h.Integral())

    h.SetLineColor(colors_lineStyles_list[i_label][0])
    h.SetLineStyle(colors_lineStyles_list[i_label][1])
    h.SetLineWidth(3)

    list_histograms_to_compare.append((h,label))

    f.Close()

  file_name='{}/{}_{}'.format(output_path,observable,channel)
  if do_weighted:
    file_name+='_weighted'
  if do_shape_only:
    file_name+='_shapeOnly'
  if do_ratio_pad:
    file_name+='_ratio'


  overlayHistograms(list_histograms_to_compare,fileName=file_name,addintegralinfo=False, doRatioPad=do_ratio_pad, ratioTitle="Ratio to first",plot_option="HIST E",plot_option_ratio="HIST E", text_option=("#bf{"+plot_label+"}",0.04,13,0.15,0.85,0.05),legend_info=legend_info)

if __name__ == "__main__":

  for observable in ['ql_cosDPlus','ql_cosDMinus']:
    if observable != 'cosTStar':
      observable_plotInfo_dict[observable]=observable_plotInfo_dict['cosTStar']
    for channel in ['wmh_e_smeftsim','wmh_mu_smeftsim','wph_e_smeftsim','wph_mu_smeftsim']:
      plotter(input_path=main_proc_dir,output_path=output_dir,
      channel=channel,observable=observable,do_shape_only=True,
      legend_info=[0.575,0.7,0.575+len(couplings_label_dir_dict)*0.03,0.7+len(couplings_label_dir_dict)*0.03,72,0.028,0],
      plot_label='Angular observable coupling sign test')
