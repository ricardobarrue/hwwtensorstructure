#!/bin/bash
#SBATCH -p lipq
#SBATCH --mem=4G

# Loading environment
module load gcc63/delphes/3.4.1

find /lstore/calo/rbarrue/MadminerWHStudies/CPoddOnly_angObs_continuityTest/ -maxdepth 5 -name "*.hepmc.gz" -exec gunzip -d {} \;

find /lstore/calo/rbarrue/MadminerWHStudies/CPoddOnly_angObs_continuityTest/ -maxdepth 5 -name "*.hepmc" | while read -r i
do
   DelphesHepMC /cvmfs/sw.el7/gcc63/delphes/3.4.1/cards/gen_card.tcl $(dirname "$i")/GEN.root $i
   #echo $(dirname "$i")
done
